Collected works, gathered into projects. No schoolwork present here to maintain academic honesty of other people working on those same assignments, available at request.

These are old, and thus not given their own repos. When I get on it I'll probably change that.

Notable works not on Bitbucket:

-A fitness-game app for android. (https://github.com/Sythe2o0/ABitFitter)

Other work available at request:

-A Memory Manager, in C, written for an application to Insomniac Games. 
-Redone Euler code in other languages (for the purpose of learning those languages)

--- See readme files within particular folders for information on those projects ---