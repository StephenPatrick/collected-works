README for CreateWebCopies.py

Dependencies:
This program has two dependencies: Python 2.7.2 and Gdata 2.0.18, python libraries for interfacing with google's programs.

Usage:
This program takes two optional command line arguments. The first is the directory which contains the images and subdirectories of images to be compressed and the second is the new directory to which the compressed images will be sent. The program does not expect a full path name but just the name of the directory from within the containing directory of CreateWebCopies.py.

Should no command line arguments be given, the user will be prompted to give the target directory to be copied from. If a second argument is not given, the directory for the copies will be determined based on the target directory.

This program completely removes everything in the directory it moves the compressed files to prior to moving the files. If the copy directory given already exists, the program will warn the user about this and give the user an opportunity to redefine the copy directory.

In order to use the compression method this program uses, a google account is needed. This is because this program compresses images by uploading them to Picasa and downloading them again. If you receive a warning that you need a Captcha to login, the following webpage as of December 2013 is the place to disable this requirement:

https://www.google.com/accounts/DisplayUnlockCaptcha

If the connection to Google is interrupted or if another error reoccurs enough times, the program will stop and ask the user whether they would like to skip the current file, keep retrying, or stop altogether. In the case of certain errors, it will immediately stop due to the suspicion they are unresolvable via retrying.

---

README for ArchiveRename.py

This program will rename all files within a file tree which begin with "_" or "s" to include their containing directory as the beginning of their filename. It currently does not take an argument.

 The justification for this naming scheme is related to the system this script was designed for.
