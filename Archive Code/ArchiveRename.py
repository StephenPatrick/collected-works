# This program renames the files in a file tree to
# be their containing directory plus their original
# file name.
# Last edited: December 30th 2013
# @Author Patrick Stephen

import os

def main():
  start_dir = os.getcwd()
  target_dir = raw_input("Please input the directory to rename the files of: ")
  directory = os.path.join(start_dir, target_dir)
  name(directory)
  print "Renaming complete."

def name(directory):
  subdirs = os.listdir(directory)

  for i in subdirs:
    path = os.path.join(directory, i)

    # Recurse on subdirectories    
    if os.path.isdir(path):
      name(path)
    else:
      (head, ext) = os.path.split(directory)
      if (i[0] == "_") or (i[0] == "s"):
        new_name = ext+i
        new_path = os.path.join(directory, new_name)
        os.rename(path, new_path)

if __name__ == "__main__":
  main()
