# This program creates web copies of TIFF files in a given directory
# and all subdirectories of said directory in parallel file structure
# The compression is done via uploading and redownloading from Picasa
# Last edited: December 30th 2013
# @Author Patrick Stephen

import os
import sys
import shutil
import socket
import urllib2
import time
from getpass import getpass
try:
  import gdata.photos.service
  from gdata.service import BadAuthentication
except ImportError:
  print "Unable to import libraries."
  print "Please ensure that Gdata version 2.0.18 (or newer) is installed."
  quit()

# Login to Picasa

gd_client = gdata.photos.service.PhotosService()

login = True

while login:
  username = raw_input("Enter Picasa email: ")
  gd_client.email = username
  gd_client.password = getpass("Enter Picasa password: ")
  gd_client.source = 'picasamasswebcopies'
  try:
    gd_client.ProgrammaticLogin()
    login = False
  except BadAuthentication:
    decision = raw_input("Invalid login. Please try again or input Q to quit: ")
    if (decision.lower() == 'q'):
      quit()

# Create an album to store the images

title = str(time.time())
album = gd_client.InsertAlbum(
  title = title, summary = "album")
album_url = '/data/feed/api/user/%s/albumid/%s' % (
  username, album.gphoto_id.text)

def main():
  start_dir = os.getcwd()

  # If the user gave no arguments, ask for the directory.

  if len(sys.argv) > 1:
    target_dir = sys.argv[1]
  else:
    target_dir = raw_input("Please input the directory to copy: ")  

  target_dir = os.path.join(start_dir,target_dir)
  
  # If the user gave a second argument, 
  # make it the duplication directory.
  # Otherwise use a default.

  if len(sys.argv) > 2:
    dips_dir = sys.argv[2]
    dips_dir = os.path.join(start_dir,dips_dir)
  else:
    dips_dir = target_dir + "_dips"

  # Determine if the duplication directory
  # already exists. 

  start_subdirs = os.listdir(start_dir) 

  (dips_head, dips_ext) = os.path.split(dips_dir)
  
  while dips_ext in start_subdirs:
    remove = True
    print '''WARNING: The web copy directory ''' + dips_dir +'''
    already exists. In order to perform this operation, the
    current web copy directory and all of its subdirectories
    will be deleted and recreated with a parallel file system
    to the target directory. Files will not be able to be recovered. 
    ''' 
    decision = raw_input("Continue? (Y/N) ").lower()
    if decision[0] != "y":
      decision = raw_input('''Would you like to name a new 
        dupilicates directory? (Y/N) ''')
      if decision[0] != "y":
        print "Exiting program."
        quit()
      else:
        dips_dir = raw_input("Please input a new directory name: ")
        dips_dir = os.path.join(start_dir,dips_dir)
        (dips_head, dips_ext) = os.path.split(dips_dir)
        remove = False

    # Remove the duplicate file tree
    if remove:
      shutil.rmtree(dips_dir)
      break
  
  compress(target_dir, dips_dir)
  print "Compression complete."


def compress(cur_dir, dips_dir):

  # Make the duplicate directory
  os.mkdir(dips_dir)

  # Generate a list of everything in the directory
  cur_subdirs = os.listdir(cur_dir)
  
  for i in cur_subdirs:
    path = os.path.join(cur_dir,i)

    # Recurse on new subdirectories
    if os.path.isdir(path):
      compress(path, os.path.join(dips_dir,i))
    else:
      if ((i[-4:] == ".tif") or (i[-5:] == ".tiff")):

        # Upload the current image
        print "Uploading " + path
        uploading = True
        downloading = True
        ercount = 0
        while (uploading):
          try:
          #Todo: Implement Timeout Protection
            photo = gd_client.InsertPhotoSimple(album_url, 'photo', 
              'Uploaded using the API', path, content_type='image/jpeg')
            uploading = False
          except (gdata.photos.service.GooglePhotosException, socket.error) as e:
            if (e[0] == 400):
              print "400 Error. This usually signifies an unresolvable error."
              ercount = 9
            ercount += 1
            # In case of a server issue, allow
            # the user to retry after reconnecting, or skip a file
            # which is problematic. 
            if (ercount == 10):
              print "Too many errors."
              decision = raw_input("Input R to retry, N to skip this file, or Q to quit ")
              if decision.lower() == "n":
                downloading = False
                break
              else: 
                if decision.lower() == "q":
                  quit()
                else:
                  ercount = 0
            else:
              print "Server error. Retrying."
              
        if not downloading:
          continue

        print "Downloading " + path
        # Redownload the image
        # Connect to google and request the data
        request = urllib2.Request(photo.content.src)
        response = urllib2.urlopen(request)
        data = response.read()
        # Correct the file extension
        if (i[-3:] == "tif"):
          filename = i[:-3] + "jpg"
        else: 
          filename = i[:-4] + "jpg"
        # Write the file
        writefile = open(os.path.join(dips_dir,filename), 'wb')
        writefile.write(data)

if __name__ == "__main__":
    main()
