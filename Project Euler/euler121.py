import sys
import copy
from fractions import Fraction
from time import clock

def main(turns):
	start = clock()

	chart_row = [0]*(int(sys.argv[1])+1)
	chart = []

	chart.append(copy.copy(chart_row))
	chart[0][0] = Fraction(1,2)
	chart[0][1] = Fraction(1,2)

	for turn in xrange(1,turns):
		chart.append(copy.deepcopy(chart_row))
		for b in xrange(turn+1):
			chart[turn][b] += chart[turn-1][b] * Fraction((turn+1),(turn+2))
			chart[turn][b+1] += chart[turn-1][b] * Fraction(1,(turn+2))

	total = Fraction(0,1)
	for b in xrange((turns/2)+1,turns+1):
		total += chart[turns-1][b]

	print total.denominator / total.numerator
	print (clock() - start)

if __name__ == '__main__':
	main(int(sys.argv[1]))