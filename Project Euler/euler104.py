import sys
from time import clock
from math import floor, log10

def isPandigital(s):
	# t = frozenset(s)
	# return len(t) == len(s) and not '0' in t

	counts = {}
	for c in s:
		if c in counts:
			return False
		counts[c] = True
	return not '0' in counts

def intLen(n):
	return long(floor(log10(n)))

def hasPandigitalEnds(n):
	return isPandigital(str(n % 1000000000)) and isPandigital(str(n / pow(10,(intLen(n)-8)))) 

def fib_gen():
	x = 1
	y = 1
	while True:
		yield x
		x, y = y, x+y

def main():

	j = 1

	f = fib_gen()
	# Skip until we've got numbers with 9 digits
	for i in xrange(50):
		next(f)
		j += 1

	n = next(f)
	while not hasPandigitalEnds(n):
		n = next(f)
		j += 1
		print j
	print j

if __name__ == '__main__':
	s = clock()
	main()
	print (clock() - s)