package main

import "fmt"
import "math"

// Didn't write this, have written this function too many times
// in too many languages to bother again
func findDivisors(num int) []int {
	divisors := []int{}
	// Speed optimization: Find all factors until root
	for i := 1; i <= int(math.Sqrt(float64(num)))+1; i++ {
		if num%i == 0 {
			divisors = append(divisors, i)
		}
	}
	// Then calculate complimentary factors and add them.
	hdiv := []int{}
	for _, v := range divisors {
		if (num / v) != v {
			hdiv = append(hdiv, (num / v))
		}
	}
	for _, v := range hdiv {
		divisors = append(divisors, v)
	}
	// And clean up so we only have unique factors.
	return removeDuplicates(divisors)
}

func removeDuplicates(a []int) []int {
	result := []int{}
	seen := map[int]int{}
	for _, val := range a {
		if _, ok := seen[val]; !ok {
			result = append(result, val)
			seen[val] = val
		}
	}
	return result
}

func main() {
	non_abundant_sum := 0
	abundant := []int{}
	for i := 1; i <= 28123; i++ {
		divisors := findDivisors(i)
		sum := 0
		for j := 0; j < len(divisors); j++ {
			sum += divisors[j]
		}
		if sum > 2*i {
			abundant = append(abundant, i)
		}
		failed := false
		for j := 0; j < len(abundant); j++ {
			// Could turn this loop into a map
			for k := j; k < len(abundant); k++ {
				if abundant[k]+abundant[j] == i {
					failed = true
					break
				} else if abundant[k]+abundant[j] > i {
					break
				}
			}
			if failed {
				break
			}
		}
		if !failed {
			fmt.Println(i)
			non_abundant_sum += i
		}

	}
	fmt.Println(non_abundant_sum)
}
