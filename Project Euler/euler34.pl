use warnings;
use strict;
use v5.10;
use Math::BigInt;

sub Digit_Factorial_Sum {
	my $x = shift;
	my $sum = 0;
	for (my $i = 0;$i < length substr ($x,0);$i++) {
		my $n = substr ($x,$i,1);
		$n = Math::BigInt::bfac($n);
		$sum += $n;
		if ($sum > $x) {return 0}		}
	return $sum;}
my $sum = 0;
my @num_ray = ();
for (my $i = 144;$i < 100000;$i++) {
	$sum = Digit_Factorial_Sum($i);
	say $i;
	if ($sum == $i) {
		push @num_ray, $i;		}} 
$sum = 0;
say scalar @num_ray;
grep {$sum += $_} @num_ray;
say $sum;