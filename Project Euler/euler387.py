from primefac import isprime
import sys
from time import clock
import profile

def isHarshad(n):
	s = sumDigits(n)
	return not n % s

def sumDigits(n):
	r = 0
   	while n:
 	   r, n = r + n % 10, n / 10
   	return r

# Takes a limit in number of digits
def rightTruncatables(limit):
	rth_prev = [1,2,3,4,5,6,7,8,9]
	rth = []
	rth_ret = []
	i = 10
	digits = 1
	while digits < limit:
		for i in rth_prev:
			for j in xrange(10):
				k = j + (i * 10)
				if isHarshad(k):
					rth.append(k)
		digits += 1
		rth_prev = rth
		rth_ret += rth
		rth = []
	return rth_ret

def main(limit):
	rth = rightTruncatables(limit)
	total = 0
	for i in rth:
		if isprime(i / sumDigits(i)):	
			for j in [1,3,7,9]:
				k = j + (i * 10)
				if isprime(k):
					print k
					total += k
	print total

if __name__ == '__main__':
	s = clock()
	main(int(sys.argv[1]))
	print (clock() - s)