package main

import "fmt"
import "strconv"
import "os"
import "bufio"
import "strings"
import "time"
import "math"

type Vertex struct {
	Id      int
	Strecke float64
	Arcs    map[int]float64
	Vorh    int
	Besucht bool
}

func timeTrack(start time.Time, name string) {
	elapsed := time.Since(start)
	fmt.Printf("%s took %s\n", name, elapsed)
}

func g_atoi(s string) int {
	i, _ := strconv.Atoi(s)
	return i
}

func main() {
	defer timeTrack(time.Now(), "euler83")
	file, _ := os.Open("euler81-input.txt")

	table := [80][80]int{}
	y := 0
	x := 0

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		words := strings.Fields(scanner.Text())
		for _, word := range words {
			table[y][x] = g_atoi(word)
			x++
		}
		x = 0
		y++
	}
	vertices := make(map[int]Vertex)

	for x := 0; x <= 79; x++ {
		for y := 0; y <= 79; y++ {
			index := y + x*100
			arcs := make(map[int]float64)
			if x != 0 {
				arcs[index-100] = float64(table[y][x])
			}
			if x != 79 {
				arcs[index+100] = float64(table[y][x])
			}
			if y != 79 {
				arcs[index+1] = float64(table[y][x])
			}
			if y != 0 {
				arcs[index-1] = float64(table[y][x])
			}
			vertices[index] = Vertex{index, math.Inf(1), arcs, -1, false}
		}
	}
	// Set up nodes for start and end points-- initial cost for start
	// and zero cost for end
	vertices[10001] = Vertex{10001, math.Inf(1), map[int]float64{}, -1, false}
	startarcs := make(map[int]float64)
	for id := 0; id <= 79; id++ {
		startarcs[id] = float64(table[0][id])
		if id != 0 {
			vertices[7900+id].Arcs[10001] = vertices[7900+id].Arcs[7900+id-1]
		} else {
			vertices[7900+id].Arcs[10001] = vertices[7900+id].Arcs[7900+id+1]
		}
	}
	vertices[10000] = Vertex{10000, 0, startarcs, -1, false}

	mussen_besuchen := len(vertices)

	for mussen_besuchen > 0 {
		naechst := Vertex{1000000, math.Inf(1), map[int]float64{}, -1, false}
		// linear and terrible
		for _, vertex := range vertices {
			if vertex.Strecke <= naechst.Strecke && !vertex.Besucht {
				naechst = vertex
			}
		}
		naechst.Besucht = true
		vertices[naechst.Id] = naechst
		fmt.Println(mussen_besuchen)
		mussen_besuchen--

		for nachbarId, strecke := range naechst.Arcs {
			nachbar := vertices[nachbarId]
			gesamt := strecke + naechst.Strecke
			if gesamt < nachbar.Strecke {
				nachbar.Strecke = gesamt
				nachbar.Vorh = naechst.Id
				if nachbar.Besucht {
					mussen_besuchen++
					nachbar.Besucht = false
				}
				vertices[nachbarId] = nachbar
			}
		}
	}
	for i := 7900; i < 7980; i++ {
		fmt.Println(vertices[i])
	}
	bevor := vertices[10001].Vorh
	for bevor != -1 {
		fmt.Println(vertices[bevor].Arcs)
		bevor = vertices[bevor].Vorh
	}
	fmt.Println(vertices[10001])
}
