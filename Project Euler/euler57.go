package main

import "fmt"
import "math/big"

func main() {

	longer_numerators := 0

	two := big.NewRat(int64(2), int64(1))
	one := big.NewRat(int64(1), int64(1))

	for i := 1; i < 1000; i++ {
		val := big.NewRat(int64(1), int64(2))
		for j := 0; j < i; j++ {
			val.Add(val, two)
			val.Inv(val)
		}
		val.Add(one, val)
		num := val.Num().String()
		denom := val.Denom().String()
		if len(num) > len(denom) {
			longer_numerators += 1
		}
	}

	fmt.Println(longer_numerators)
}
