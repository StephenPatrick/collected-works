use strict;
use warnings;
use v5.14;
use bigint;

sub wordVal {
	my $word = substr $_[0], 1, -1;
	my $val = 0;
	my @chars = split //,$word;
	$val += ord($_)-64 for @chars;
	return $val;
}

# Open the list of names
open my $file, "p022_names.txt";
# Split the name list into an array
my @names = split /,/, <$file>;

my $result = 0;
my @sortedNames = sort(@names);
for (my $i = 0; $i < scalar @sortedNames; $i++){
	$result += ($i+1) * wordVal($sortedNames[$i]);
}
say $result;