solution_row = [-1]*200
solution = [solution_row]*200

#[k][j], k < j

# 1
solution[0][1] = 0
contains[0][1] = {}

# 2
solution[1][1] = 1
contains[1][1] = {}

# 3
solution[1][2] = 2
contains[1][2] = merge_d(contains[1][1],{1+1:True, 3:True})

# 4
solution[1][3] = 3
contains[1][3] = merge_d(contains[1][2],{1+2:True, 4:True})
solution[2][2] = 2
contains[2][2] = merge_d(contains[1][1],{1+1:True, 4:True})

# 5
solution[1][4] = 3
contains[1][4] = merge_d(contains[2][2],{2+2:True, 5:True}) 
solution[2][3] = 3
contains[2][3] = merge_d(merge_d(contains[1][2],contains[1,1]),{2+3:True, 5:True})

# 6
solution[1][5] = 4
solution[2][4] = 3
solution[3][3] = 3

# 7
solution[1][6] = 4
solution[2][5] = 4
solution[3][4] = 4

# 8
solution[1][7] = 5
solution[2][6] = 4
solution[3][5] = 4
solution[4][4] = 3

#9
solution[1][8] = 4
solution[2][7] = 5
solution[3][6] = 4
solution[4][5] = 4

#10
solution[1][9] = 5
solution[2][8] = 5
solution[3][7] = 5
solution[4][6] = 4
solution[5][5] = 4

res_memo = {1:[0,[0]],
			2:[1,[1]],
			3:[2,[1]],
			4:[2,[2]],
			5:[3,[1,2]],
			6:[3,[2,3]],
			7:[4,[1,2,3]],
			8:[3,[4]],
			9:[4,[1,3,4]],
			10:[4,[4,5]]}

def solution_results(n):
	if n in res_memo:
		return res_memo[n]
	min_val = 100000000
	results = []
	for i in range(1,(n/2)+1):
		v = solution[i][n-i]
		if v == -1:
			print "Uninitialized solution access at " + i + "," + n-i
		if v < min_val:
			results = [i]
			min_val = v 
		else if v == min_val:
			results += i
	res_memo[n] = [min_val, results]
	return res_memo[n]


def minimal_additions(n):
	for i in range(1,(n/2)+1):
		res1 = solution_results(n-i) + 1
		total = res1[0]
		contained = False 
		for j in res1[1]:
			k = n - j
			if i in contains[j][k]:
				contained = True
		res2 = solution_results(i)
		if not contained:
			total += res2[0]

def main():
	# s = 0
	# for i in range(1,20):
	# 	(j, _) = minimal_additions(i, {})
	# 	s += j
	# print s


if __name__ == "__main__":
	main()