package main

import "fmt"
import "math"

// Send the sequence 2, 3, 4, ...
func Generate(ch chan<- int) {
	for i := 2; ; i++ {
		ch <- i
	}
}

// Copy the values from channel in to channel out,
// removing those divisible by prime.
func Filter(in <-chan int, out chan<- int, prime int) {
	for {
		i := <-in
		if i%prime != 0 {
			out <- i
		}
	}
}

func Next_Prime(ch <-chan int) (prime int, new_ch chan int) {
	prime = <-ch
	new_ch = make(chan int)
	go Filter(ch, new_ch, prime)
	return
}

func main() {
    
	primes := []float64{}
	ch := make(chan int)
	a := 0
	go Generate(ch)
    // sqrt(50 mil ~~ 7100)
	for a <= 7500 {
		fmt.Println(a)
		// Get the next prime, update
		// our prime generation channel.
		a, ch = Next_Prime(ch)
        primes = append(primes, float64(a))
	}
    power_triples := 0
    found := map[int]bool{}
    for _,square := range primes { square = math.Pow(square,2) 
        for _,cube := range primes { cube = math.Pow(cube,3)  
            if (cube + square > 50000000) { break }
            for _,tess := range primes { tess = math.Pow(tess,4) 
                total := square + cube + tess
                if (total > 50000000) { break 
                } else if  _,ok := found[int(total)]; !ok {
                        found[int(total)] = true
                        power_triples++
                }
            }
        }
    }
    fmt.Println(power_triples)
}