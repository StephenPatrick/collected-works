import groovy.transform.TailRecursive
import groovy.transform.Memoized

class euler78 {

    def Pmemo = {n, m->
        if (m == 0G || n < 0G) return 0G
        if (n < 2G || m == 1G) return 1G
        return Pmemo(n,m-1G) + Pmemo(n-m,m) % 1000000;
    }.memoize()

    @TailRecursive
    BigInteger P(BigInteger n, BigInteger m, BigInteger total) { 
        if (m == 0G || n < 0G) return total % 1000000
        if (n < 2G || m == 1G) return total+1G % 1000000
        return P(n,m-1G,total+Pmemo(n-m,m));
    }

    def run ={-> 
        BigInteger x = 1G
        BigInteger i = 2G
        while (x % 1000000 != 0){
            x = P(i,i,0G) % 1000000
            i++
            print i + " "
            println x
            if (i > 1000) return
        }
    }

    def newP = {n->
        if (n < 0G) return 0G
        if (n < 2G) return 1G
        sum = 0
        for (k = 1; k <= n; k++) {
            partial = (-1)**(k+1)
            left = newP(n-((1/2)*(k)*((3*k)-1)))
            right = newP(n-((1/2)*(k)*((3*k)+1)))
            partial *= (left + right)
            sum += partial
        }
        return sum
    }
}