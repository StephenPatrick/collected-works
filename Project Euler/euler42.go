package main

import "fmt"
import "strconv"
import "os"
import "bufio"
import "strings"

func g_atoi(s string) int {
	i, _ := strconv.Atoi(s)
	return i
}

//func contains(slice int[], val int) bool{
//    start := 0;
//    max := len(slice);
//
//}

func main() {
	triangle := triangle_numbers(40)
	file, _ := os.Open("euler42-input.txt")

	scanner := bufio.NewScanner(file)
	scanner.Scan()
	count := 0
	words := strings.Fields(scanner.Text())
	for _, word := range words {
		val := word_val(word)
		fmt.Println(val, word)
		if triangle[val] {
			count++
		}
	}
	fmt.Println(count)

}

func triangle_numbers(max int) map[int]bool {
	returnVal := make(map[int]bool)
	currentNum := 1
	index := 1
	for index < (max + 1) {
		returnVal[currentNum] = true
		index++
		currentNum = (index) * (index + 1) / 2
	}
	return returnVal
}

func char_val(c byte) int {
	if !(c > 64 && c < 91) {
		return 0
	}
	return int(c - 64)
}

func word_val(word string) int {
	sum := 0
	i := 0
	for i < len(word) {
		sum += char_val(word[i])
		i++
	}
	return sum
}
