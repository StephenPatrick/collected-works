rectCount = {x,y->
    return ((y * (y + 1)) / 2) * (x * (x +1)) / 2
}

benchmark = { closure ->  
  start = System.currentTimeMillis()  
  closure.call()  
  now = System.currentTimeMillis()  
  now - start  
}  

euler85 = {->
    int best = 2000000
    int best_w = 0
    int best_h = 0
    int x = 1
    int y = 1
    columnChecked = false
    while (x < 100) {
        next = rectCount(x,y)
        diff = (2000000 - next).abs() 
        if (diff < best) {
            best = diff
            best_w = x
            best_h = y
        }
        if (next > 2000000) {
            y--
            // Only proceed to the next column once we've dropped
            // down enough from the last column's height to be 
            // below two million again.
            if (columnChecked) {
                columnChecked = false
                x++
            }
        } else {
            y++
            columnChecked = true
        }
    }
    println (best_w * best_h)
}

benchmark(euler85)