use warnings;
use strict;

my @bool_primes = Eratosthenes(10000);
my @bool_d_squares = double_squares(10000);
my @primes;
my @d_squares;
for (my $i = 0; $i < scalar @bool_primes; $i++){
	if ($bool_primes[$i] == 1){
		push @primes, $i;
	}
} 
for (my $i = 0; $i < scalar @bool_d_squares; $i++){
	if ($bool_d_squares[$i] == 1){
		push @d_squares, $i;
	}
} 
my $pvalue = 0;
my $value = 0;
my $j = 0;
my $k = 0;
for (my $i = 9; $i < scalar @bool_primes; $i += 2){
	if ($bool_primes[$i] == 0){
		while ($pvalue < $i && $value != $i){
			$pvalue = $primes[$j];
			$value = 0;
			#print "p = $pvalue\n";
			while ($value < $i){
				$value = $pvalue + $d_squares[$k];
				#print "v = $value\n";
				$k++;
			}
			$k = 0;
			$j++;
		}
		if ($value == $i){#print $i."\n";
		}
		else { print $i."\n"; my $wait = <>;}
		$pvalue = 0;
		$value = 0;
		$j = 0;	
		$k = 0;
	}
}


sub double_squares{
	my $n = shift;
	my @doubles = (0) x ($n);
	for (my $i = 1; $i < sqrt ($n / 2); $i++){
		print $i."\n";
		$doubles[2*($i**2)] = 1;
	}
	return @doubles;
	
}

sub Eratosthenes{
	my $n = shift;
	my @primes = (0,0);
	my @addon = (1) x ($n - 2);
	push @primes, @addon;
	for (my $i = 2; $i <= sqrt $n; $i++){
		print $i."\n";
		if ($primes[$i] == 1){
			for (my $j = $i**2; $j <= $n; $j += $i){
				$primes[$j] = 0;
			}
		}
	}
	return @primes;
}