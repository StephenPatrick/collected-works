-module(euler1_21).
-export([euler_1/0,euler_2/0]).
-import(lists, [sum/1]).

euler_1() ->
	Sum3 = sum_to((999) div 3) * 3,
	Sum5 = sum_to((999) div 5) * 5,
	Sum15 = sum_to((999) div 15) * 15,

	[Sum3 + Sum5 - Sum15].

euler_2() ->
	sum(even(fibo([1,0]))).

even(L) ->
	[X || X <- L, X rem 2 =:= 0].

fibo([H,B|T]) when H+B =< 4000000 -> fibo([H+B,H,B|T]);
fibo(L) -> L.

sum_to(X) ->
	X * (X + 1) / 2.