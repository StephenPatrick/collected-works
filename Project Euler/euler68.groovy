import groovy.util.GroovyCollections

magicRing = {n->
    ints = new ArrayList<Integer>()
    for (i = 1; i <= n*2; i++) {
        ints.add(i)
    }
    result = []
    ints.eachPermutation {combo->
        boolean success = true
        int goal = combo[0] + combo[n] + combo[n+1]
        int l;
        for (i = 0; i < n; i++) {
            if (i == n-1) {
                l = i+1
            } else {
                l = n+i+1
            }
            if (combo[i] + combo[n+i] + combo[l] != goal) {
                success = false
                break
            }
        }
        if (success) {
            outer = combo[0..n-1]
            inner = combo[n..-1]
            // check if this is a rotation
            for (perm in result) {
                outer_2 = perm[0..n-1]
                inner_2 = perm[n..-1]
                // Rotated right
                outer_2_right = outer_2[1..-1] + [outer_2[0]]
                inner_2_right = inner_2[1..-1] + [inner_2[0]]
                // Rotated left
                outer_2_left = [outer_2[n-1]] + outer_2[0..n-2]
                inner_2_left = [inner_2[n-1]] + inner_2[0..n-2]
                if ((outer == outer_2_right && inner == inner_2_right) ||
                    (outer == outer_2_left && inner == inner_2_left)) {
                    success = false
                    break;
                } 
            }
            if (success) result << combo;
        } 
    }
    return result
} 

// We need to add a step which rotates all remaining rings
// So they have their smallest element in position 0.
// I picked the solution by hand out of the result from above.
maxRingToString = {rings ->
    // Get the max ring
    int maxRing = 0G
    maxRingString = "0"
    for (ring in rings) {
        n2 = (int)(ring.size()/2)
        ringString = ""
        for (i = 0; i < n2; i++) {
            ringString += ring[i]
            ringString += ring[i+n2]
            if (i == n2-1) {
                ringString += ring[i+1]
            } else {
                ringString += ring[i+n2+1]
            }
        }
        if (ringString.toBigInteger() > maxRing) {
            maxRing = ringString.toBigInteger()
            maxRingString = ringString
        }
    }  
    return maxRingString
}