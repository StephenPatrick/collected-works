import sys
from time import clock

# Limit in terms of 2^limit
def main(limit):
	total = 0
	for n in xrange(1,(2**limit)+1):
		if not (n ^ 2*n ^ 3*n):
			total += 1
	print total

if __name__ == '__main__':
	s = clock()
	main(int(sys.argv[1]))
	print (clock() - s)