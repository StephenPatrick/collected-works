lengths = [:]
total = 0

deepcopy = {orig->
     bos = new ByteArrayOutputStream()
     oos = new ObjectOutputStream(bos)
     oos.writeObject(orig); oos.flush()
     bin = new ByteArrayInputStream(bos.toByteArray())
     ois = new ObjectInputStream(bin)
     return ois.readObject()
}

vByMX = {v,orig->
    def vec = v.clone()
    def matrix = deepcopy(orig)
    for (int y = 0; y < vec.size(); y++) {
        for (x = 0; x < matrix[0].size(); x++) {
            matrix[y][x] *= vec[y]
        }    
    }
    def ret = []
    for (int y = 0; y < matrix.size(); y++) {
        int sum = 0
        for (x = 0; x < matrix[0].size(); x++) {
            sum += matrix[x][y]
        }
        ret += sum
    }
    return ret
}

U = [[1,2,2],
     [-2,-1,-2],
     [2,2,3]]
A = [[1,2,2],
     [2,1,2],
     [2,2,3]]
D = [[-1,-2,-2],
     [2,1,2],
     [2,2,3]]

euler75 = {vec->
    int magn = vec[0]+vec[1]+vec[2]
    if (magn > 1600000) {
        return
    }
    euler75(vByMX(vec,A))
    euler75(vByMX(vec,U))
    euler75(vByMX(vec,D))
    int x = vec[0]
    int y = vec[1]
    int z = vec[2]
    while (magn < 1500000) {
        if (lengths[magn] && lengths[magn] == 1) {
            lengths[magn] = 2
            total--
        } else if (!lengths[magn]) {
            lengths[magn] = 1
            total++
        } 
        // This could be replaced with product addition to total
        vec[0] = vec[0]+x
        vec[1] = vec[1]+y
        vec[2] = vec[2]+z
        magn = vec[0]+vec[1]+vec[2]
        
    }
}

euler75([3,4,5])
println total