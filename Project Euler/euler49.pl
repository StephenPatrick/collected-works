use warnings;
use strict;
use v5.10;

sub Tri_Permutation_Test {
	my $n1 = shift;
	my $n2 = shift;
	my $n3 = shift;
	my @ar1 = ();
	my @ar2 = ();
	my @ar3 = ();
	my $sum1 = 0;
	my $sum2 = 0;
	my $sum3 = 0;
	for (my $i=0;$i < length $n1;$i++) {
		push @ar1, substr ($n1,$i,1);
		$sum1 += substr ($n1,$i,1);
		#say substr ($n1,$i,1);
		#my $wait = <>;
	}
	for (my $i=0;$i < length $n2;$i++) {
		push @ar2, substr ($n2,$i,1);
		$sum2 += substr ($n2,$i,1);
		#say substr ($n2,$i,1);
		#grep {say $_} @ar1;
		#my $wait = <>;
		if (!(substr ($n2,$i,1) ~~ @ar1)) {
			#say "HELLO";
			return 0;
		} 
	}
	for (my $i=0;$i < length $n3;$i++) {
		push @ar3, substr ($n3,$i,1);
		$sum3 += substr ($n3,$i,1);
		#say substr ($n3,$i,1);
		#grep {say $_} @ar1;
		#my $wait = <>;
		if (!(substr ($n3,$i,1) ~~ @ar1)) {
			return 0;
		}
	}
	if ($sum1 != $sum2 || $sum1 != $sum3) {
		return 0;
	}
	return 1;
	
}

my @four_digit_primes = ();
my @sequential_primes = ();
my @prime_permutations = ();
my $max = 0;
my $prime = 1;
for (my $i=1000;$i<10000;$i++) {
	for (my $j=2;$j<=sqrt $i;$j++) {
		if (($i % $j) == 0) {
			$prime = 0;
			last;
		}
	}
	if ($prime > 0) {
		push @four_digit_primes, $i;
		next;
	}
	$prime = 1;
}
grep {say $_} @four_digit_primes;
grep { 
	$max = 10000 - $_;
	for (my $i=1;$i<=$max / 2;$i++) {
		my $n = $_ + $i;
		my $n2 = $n + $i;
		if (($n ~~ @four_digit_primes) && ($n2 ~~ @four_digit_primes)) {
			if (Tri_Permutation_Test($_,$n,$n2) == 1) {	
				say $_.", ".$n.", ".$n2;
				push @sequential_primes, $_; push @sequential_primes, $n; push @sequential_primes, $n2;
			}
		}
	}
	
} @four_digit_primes;
