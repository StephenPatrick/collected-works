use warnings;
use strict;

my @bool_primes = Eratosthenes(1000000);
my @primes;
for (my $i = 0; $i < scalar @bool_primes; $i++){
	if ($bool_primes[$i] == 1){
		push @primes, $i;
	}
} 
my $prime;
my $count = 0;
my $p_count = 0;
my $sum = 0;
my $j = 0;
my $running = 1;
my $i = 0;
while ($running){
	#print $sum."\n";
	if ($sum < 1000000){
		if ($sum == 997651){
			print $count;
			my $wait = <>;
		}
		$sum += $primes[$i];
		if ($sum < 1000000 && $bool_primes[$sum] == 1 && $count > $p_count){
			$p_count = $count;
			$prime = $sum;
		}
		$i++;
		if ($i >= scalar @primes){
			$running = 0;
		}
		$count++;
	}
	else {
		$sum -= $primes[$j];
		if ($sum < 1000000 && $bool_primes[$sum] == 1 && $count > $p_count){
			$p_count = $count;
			$prime = $sum;
		}
		$j++;
		$count--;
	}
}
print $prime."\n";
print $p_count."\n";


sub Eratosthenes{
	my $n = shift;
	my @primes = (0,0);
	my @addon = (1) x ($n - 2);
	push @primes, @addon;
	for (my $i = 2; $i <= sqrt $n; $i++){
		print $i."\n";
		if ($primes[$i] == 1){
			for (my $j = $i**2; $j <= $n; $j += $i){
				$primes[$j] = 0;
			}
		}
	}
	return @primes;
}