use warnings;
use strict;

my @bool_primes = Eratosthenes(1000000);
my @primes = ();
for (my $i = 0; $i < scalar @bool_primes; $i++){
	if ($bool_primes[$i] == 1){
		push @primes, $i;
	}
}
my @out = ();
my $s_prime;
my $iterating = 1;
for (my $i = 0; $i < scalar @primes; $i++){
	$s_prime = $primes[$i];
	while ($iterating == 1){	
		$s_prime = dig_shift_left($s_prime,1);
		if ($bool_primes[$s_prime] != 1){
			$iterating = -1;
		}
		if ($s_prime == $primes[$i]){
			$iterating = 0;
		}
	}
	if ($iterating == 0){
		push @out, $primes[$i];
	}
	$iterating = 1;
}

foreach (@out){ print $_."\n";}
print "Answer: ".scalar @out."\n";

sub dig_shift_left{
	my ($n, $shift) = @_;
	my $out = "";
	my $index;
	for (my $i = 0; $i < length $n; $i++){
		$index = ($i + $shift) % (length $n);
		$out .= substr $n, $index, 1; 
	}
	return $out;
}

sub Eratosthenes{
	my $n = shift;
	my @primes = (0,0);
	my @addon = (1) x ($n - 2);
	push @primes, @addon;
	for (my $i = 2; $i <= sqrt $n; $i++){
		print $i."\n";
		if ($primes[$i] == 1){
			for (my $j = $i**2; $j <= $n; $j += $i){
				$primes[$j] = 0;
			}
		}
	}
	return @primes;
}