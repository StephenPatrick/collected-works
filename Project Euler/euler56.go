package main

import "fmt"
import "math/big"
import "time"

func timeTrack(start time.Time, name string) {
	elapsed := time.Since(start)
	fmt.Printf("%s took %s\n", name, elapsed)
}

func main() {
	defer timeTrack(time.Now(), "euler56")

	best_sum := 0
	for i := 0; i < 100; i++ {
		for j := 0; j < 100; j++ {
			a := big.NewInt(int64(i))
			b := big.NewInt(int64(j))
			val := a.Exp(a, b, nil).String()
			sum := 0
			for _, c := range val {
				sum += int(c - '0')
			}
			if sum > best_sum {
				best_sum = sum
			}
		}
	}
	fmt.Println(best_sum)
}
