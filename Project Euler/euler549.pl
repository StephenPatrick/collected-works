use strict;
use warnings;
#use bigint;
use v5.14;

# Generate a divisor list and sum of all divisors for a
# given integer.
# Input: int, desired output
# Output: A) int sum of all divisors,
#	  B) int array of divisors,
#	  C) both A and B
sub divisors{
	my $base = shift;
	my @divisors = ();
	my $k = 0;
	
	# Add all factors under the square root of the base
	for (my $j=1;$j<sqrt $base + 1;$j++) {
		if ($base % $j == 0) {
			$divisors[$k] = $j;
			$k++;
		}
	}
	# Determine all factors paired with those under the
	# square root.
	for (my $j=1;$j<(scalar @divisors);$j++) {
		# Exclude duplicate squares
		if (!($base / $divisors[$j] ~~ @divisors)) {
			$divisors[$k] = $base / $divisors[$j];
			$k++;
		}
	}

	return @divisors;
}

my @factorials = (1);
my $val = 1;
for (my $i = 2; $i < 10; $i++) {
	$factorials[++$#factorials] = $val;
	$val *= $i;
	say $val;
}
my @fact_divisors = ();
foreach my $fact (@factorials) {
	@fact_divisors[++$#fact_divisors] = divisors($fact)
}
my @fact_divisors = map {divisors($_)} @factorials;
for (my $i = 0; $i < 9; $i++) {
	say $factorials[$i];
	say $fact_divisors[$i];
}