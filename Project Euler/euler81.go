package main

import "fmt"
import "strconv"
import "os"
import "bufio"
import "strings"
import "math"

func g_atoi(s string) int {
	i, _ := strconv.Atoi(s)
	return i
}

func main() {
	file, _ := os.Open("euler81-input.txt")

	table := [80][80]int{}
	y := 0
	x := 0

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		words := strings.Fields(scanner.Text())
		for _, word := range words {
			table[y][x] = g_atoi(word)
			x++
		}
		x = 0
		y++
	}
	best_table := [80][80]int{}

	x = 79
	y = 79
	for x := 79; x >= 0; x-- {
		for y := 79; y >= 0; y-- {
			// The only way you can go is right
			if y == 79 {
				// Origin point
				if x == 79 {
					best_table[y][x] = table[y][x]
				} else {
					best_table[y][x] = table[y][x] + best_table[y][x+1]
				}
				// The only way you can go is down
			} else if x == 79 {
				best_table[y][x] = table[y][x] + best_table[y+1][x]
				// Use the lower of the two options
			} else {
				best_table[y][x] = table[y][x] + int(math.Min(float64(best_table[y+1][x]),
					float64(best_table[y][x+1])))
			}
		}
	}
	fmt.Println(best_table[0][0])
}
