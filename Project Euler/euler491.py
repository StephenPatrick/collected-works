import itertools

def arrtoi(arr):
	i = 0
	for n in arr:
		i = (i * 10) + n
	return i

def main():
	total = 0
	digits_twice = [0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9]
	for dbl_p in itertools.permutations(digits_twice):
		if dbl_p[0] == 0:
			continue
		print dbl_p
		if not arrtoi(dbl_p) % 11:
			print dbl_p
			total += 1
	return total

if __name__ == '__main__':
	main()