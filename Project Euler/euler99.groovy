import java.math.BigInteger

best = 0G
file = new File("euler99-input.txt")
i = 1
file.eachLine { line ->
    line = line.split(",")
    base = Math.log(new BigInteger(line[0]))
    exp = new BigInteger(line[1])
    res = base*exp
    if (res > best) {
        best = res
        best_i = i
    }
    i++
}
println best_i