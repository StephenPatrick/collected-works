use warnings;
use strict;

my @counts = (0) * 1001;
for (my $a = 1; $a < 500; $a++){
	for (my $b = $a; $b < 500; $b++){
		my $c = pythagoras($a,$b);
		if (($c == int($c)) && (($a+$b+$c) < 1001)){
			$counts[$a+$b+$c]++;
		}
	}
}
my $max = 0;
my $maxindex;
for (my $i = 0; $i < scalar @counts; $i++){
	if (defined $counts[$i] && $counts[$i] > $max){
		$max = $counts[$i];
		$maxindex = $i;
	}
}
print $maxindex."\n";

sub pythagoras{
	my ($a, $b) = @_;
	return sqrt ($a**2 + $b**2);	
}