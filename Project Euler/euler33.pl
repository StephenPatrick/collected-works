use warnings;
use strict;
use Memoize;

my ($r, $x, $y);
my ($dig_a, $dig_b);
my ($frac_1, $frac_2);
for (my $i = 10; $i < 100; $i++){
	for (my $j = $i+1; $j < 100; $j++){
		if (digit_share($i,$j) != -1){
			($r, $x, $y) = extendedEuclid($j,$i);
			if ($r != 1){
				($dig_a, $dig_b) = digit_share($i,$j);
				if ($dig_a == 1){
					$dig_a = substr $i,0,1;
				}
				else {$dig_a = substr $i,1,1;}
				if ($dig_b == 1){
					$dig_b = substr $j,0,1;
				}
				else {$dig_b = substr $j,1,1;}
				$frac_1 = $i / $j;
				if ($dig_b != 0){
					$frac_2 = $dig_a / $dig_b;
				}
				else {next;}
				if ($frac_1 == $frac_2){
					print "$i / $j\n";
				}
			}
		}
	}
}
sub digit_share{
	my ($a, $b) = @_;
	for (my $i = 0; $i < length $a; $i++){
		for (my $j = 0; $j < length $b; $j++){
			if ($i != $j){
				my $n = substr $a, $i, 1;
				my $m = substr $b, $j, 1;
				if ($n == $m){
					return ($i,$j);
				}
			}
		}
	}
	return -1;
}

sub extendedEuclid{
	my ($prev_r, $r) = @_;
	my $s = 0; my $prev_s = 1;
	my $t = 1; my $prev_t = 0;
	my $q = 0;
	while ($r != 0){	
		use integer;
		$q = $prev_r / $r;
		($prev_r, $r) = ($r, $prev_r - $q*$r);
		($prev_s, $s) = ($s, $prev_s - $q*$s);
		($prev_t, $t) = ($t, $prev_t - $q*$t);
	}
	return ($prev_r, $prev_s, $prev_t);
}