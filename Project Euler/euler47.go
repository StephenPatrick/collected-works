package main

import "fmt"
import "math"
import "math/big"

// Didn't write this, have written this function too many times
// in too many languages to bother again
func findDivisors(num int) []int {
	divisors := []int{}
	// Speed optimization: Find all factors until root
	for i := 1; i <= int(math.Sqrt(float64(num)))+1; i++ {
		if num%i == 0 {
			divisors = append(divisors, i)
		}
	}
	// Then calculate complimentary factors and add them.
	hdiv := []int{}
	for _, v := range divisors {
		if (num / v) != v {
			hdiv = append(hdiv, (num / v))
		}
	}
	for _, v := range hdiv {
		divisors = append(divisors, v)
	}
	// And clean up so we only have unique factors.
	return removeDuplicates(divisors)
}

func removeDuplicates(a []int) []int {
	result := []int{}
	seen := map[int]int{}
	for _, val := range a {
		if _, ok := seen[val]; !ok {
			result = append(result, val)
			seen[val] = val
		}
	}
	return result
}

func main() {
	i := 646
	chain := 0
	for true {
		divisors := findDivisors(i)
		primeCount := 0
		for _, j := range divisors {
			bigj := big.NewInt(int64(j))
			if bigj.ProbablyPrime(10) {
				primeCount++
			}
		}
		if primeCount > 3 {
			chain++
			if chain > 3 {
				fmt.Println(i-3, i-2, i-1, i)
			}
		} else {
			chain = 0
		}
		i++
	}
}
