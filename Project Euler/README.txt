This code is virtually entirely uncommented as it was produced rapidly,
the problems each file solves is already documented, functions have 
explanatory names, and there is little else that should not be self-explanatory.

Each file has a number $n which corresponds to the webpage "projecteuler.net/problem=$n"
At this webpage you will find the problem description which the code solves.

All problems up until 50 (among others, as of March 31st 2014) have been solved.
Those not present here are not present either due to their simplicity or due to
misplaced code.  