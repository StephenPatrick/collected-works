use warnings;
use strict;

my $substr;
my $no = 0;
my $also_no = 0;
my @primes = Eratosthenes(7652420);
for (my $i = 7652419; $i > 1; $i--){
	if ($primes[$i] == 1){
		print $i."\n";
		$also_no = 1;
		for (my $j = 1; $j <= length $i; $j++){
			$no = 1;
			for (my $k = 0; $k < length $i; $k++){
				$substr = substr $i, $k, 1;
				if ($substr == $j){
					$no = 0;
					last;
				}
			}
			if ($no) {
				$also_no = 0;
				last;
			}
		}
		if ($also_no){
			print $i; last;
		}
	}
}

sub isPandigital{
	my $i = shift;
	my $substr;
	my $no = 0;
	my $also_no = 1;
	for (my $j = 1; $j <= length $i; $j++){
		$no = 1;
		for (my $k = 0; $k < length $i; $k++){
			$substr = substr $i, $k, 1;
			if ($substr == $j){
				$no = 0;
				last;
			}
		}
		if ($no) {
			$also_no = 0;
			last;
		}
	}
	if ($also_no){
		return 1;
	}
	return 0;
	
}

sub Eratosthenes{
	my $n = shift;
	my @primes = (0,0);
	my @addon = (1) x ($n - 2);
	push @primes, @addon;
	for (my $i = 2; $i <= sqrt $n; $i++){
		print $i."\n";
		if ($primes[$i] == 1){
			for (my $j = $i**2; $j <= $n; $j += $i){
				$primes[$j] = 0;
			}
		}
	}
	return @primes;
}