
tileCount = {squares,length->
    if (squares < length) {
        return 0
    }
    if (squares == length) {
        return 1
    }
    // What if we put a tile at the front
    long total = tileCount((squares-length), length) + 1
    // What if we don't put a tile at the front
    total += tileCount(squares-1, length)
    return total
}.memoize()