package main

import "fmt"

//import "math/big";
import "strings"
import "sort"
import "bytes"
import "strconv"

func Get_Keys(m map[string]bool) (ret []string) {
	ret = []string{}
	for k, _ := range m {
		ret = append(ret, k)
	}
	return
}

// Send the sequence 2, 3, 4, ... to channel 'ch'.
func Generate(ch chan<- int) {
	for i := 2; ; i++ {
		ch <- i // Send 'i' to channel 'ch'.
	}
}

// Copy the values from channel 'in' to channel 'out',
// removing those divisible by 'prime'.
func Filter(in <-chan int, out chan<- int, prime int) {
	for {
		i := <-in // Receive value from 'in'.
		if i%prime != 0 {
			out <- i // Send 'i' to 'out'.
		}
	}
}

func Next_Prime(ch <-chan int) (prime_str string, new_ch chan int) {
	prime := <-ch
	new_ch = make(chan int)
	go Filter(ch, new_ch, prime)
	prime_str = strconv.Itoa(prime)
	return
}

// Sorts characters of a string
func Sort_String(w string) string {
	s := strings.Split(w, "")
	sort.Strings(s)
	return Str_Append(s...)
}

var buffer bytes.Buffer

func Str_Append(strs ...string) string {
	buffer.Reset()
	for _, str := range strs {
		buffer.WriteString(str)
	}
	return buffer.String()
}

var prime_list map[string]map[string]bool

func main() {

	prime_list := map[string]map[string]bool{}

	//  primes
	ch := make(chan int)
	a := "1"
	go Generate(ch)
	for i := 0; i < 100; i++ {
		a, ch = Next_Prime(ch)
		for b, b_match := range prime_list {
			if strings.HasPrefix(a, b) {
				if _, ok := prime_list[a[len(b):]]; ok {
					b_match[a[len(b):]] = true
				}
			}
			if strings.HasSuffix(a, b) {
				if _, ok := prime_list[a[:len(a)-len(b)]]; ok {
					b_match[a[:len(a)-len(b)]] = true
				}
			}
			if len(b_match) > 3 {
				if Check_Solution(append(Get_Keys(b_match), b)...) {
					return
				}
			}
			prime_list[b] = b_match
		}
		prime_list[a] = map[string]bool{}
	}

}

func Check_Solution(strs ...string) bool {
	fmt.Println(strs)
	valid_primes := 0
	for _, s := range strs {
		prime_is_valid := 0
		for _, check := range strs {
			if check == s {
				continue
			}
			fmt.Println("--", s, prime_list[s])
			if _, ok := prime_list[s][check]; ok {
				prime_is_valid++
			}
		}
		if prime_is_valid > 3 {
			valid_primes++
		}
	}
	fmt.Println(valid_primes)
	return (valid_primes > 4)
}
