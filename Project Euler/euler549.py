from math import floor, sqrt
from time import clock
try: 
    long
except NameError: 
    long = int
import sys
import itertools
import csv

#file = open('out.csv','wb')
#writer = csv.writer(file)
#writer.writerow(["n","s(n)"])

s_list = [None]*100000001
LIMIT = 100000001

def main():
    start = clock()
    applyPrimes(s_list)
    print S(int(sys.argv[1]))
    print (clock() - start)
    #file.close()

def s(n):
    i = max(prime_factor_set(n).keys())
    while True:
        if factorial(i) % n == 0:
            #writer.writerow([n, i])
            return i     
        i += 1

def S(n):
    l = s_list
    total = 0
    for i in range(2,n+1):
        if l[i] != None:
            #writer.writerow([n, s_list[n]])
            total += l[i]
        else:
            total += s(i)
        if i % 100000 == 0:
            print "S("+str(i) + "): " + str(total)
    return total

factorial_memo = {}
def factorial(k):
    if k < 2: return 1
    if not k in factorial_memo:
        factorial_memo[k] = k * factorial(k-1)
    return factorial_memo[k]

prime_list = []

def prime_factor_set(n):
    factors = {}
    i = 0
    d = prime_list[i]
    while n > 1:
        if n % d == 0:
            factors[d] = True
            while n % d == 0:
                n /= d
        i += 1
        d = prime_list[i]
        if d*d > n:
            if n > 1: factors[n] = True
            break
    return factors

# Not a speed bottleneck. Takes ~43 seconds. Always.
def applyPrimes(s_list):
    p = primes()
    nextPrime = next(p)
    while nextPrime < 100000001:
        prime_list.append(nextPrime)
        i = nextPrime
        while i < (nextPrime*nextPrime) and (i < 100000001):
            s_list[i] = nextPrime
            i += nextPrime
        nextPrime = next(p)

# wheel factorization prime generator
# from rosetta code
def primes():
    whlPrms = [2,3,5,7,11,13,17]                # base wheel primes
    for p in whlPrms: yield p
    def makeGaps():
        buf = [True] * (3 * 5 * 7 * 11 * 13 * 17 + 1) # all odds plus extra for o/f
        for p in whlPrms:
            if p < 3:
                continue              # no need to handle evens
            strt = (p * p - 19) >> 1            # start position (divided by 2 using shift)
            while strt < 0: strt += p
            buf[strt::p] = [False] * ((len(buf) - strt - 1) // p + 1) # cull for p
        whlPsns = [i + i for i,v in enumerate(buf) if v]
        return [whlPsns[i + 1] - whlPsns[i] for i in range(len(whlPsns) - 1)]
    gaps = makeGaps()                           # big wheel gaps
    def wheel_prime_pairs():
        yield (19,0); bps = wheel_prime_pairs() # additional primes supply
        p, pi = next(bps); q = p * p            # adv to get 11 sqr'd is 121 as next square to put
        sieve = {}; n = 23; ni = 1              #   into sieve dict; init cndidate, wheel ndx
        while True:
            if n not in sieve:                  # is not a multiple of previously recorded primes
                if n < q: yield (n, ni)         # n is prime with wheel modulo index
                else:
                    npi = pi + 1                # advance wheel index
                    if npi > 92159: npi = 0
                    sieve[q + p * gaps[pi]] = (p, npi) # n == p * p: put next cull position on wheel
                    p, pi = next(bps); q = p * p  # advance next prime and prime square to put
            else:
                s, si = sieve.pop(n)
                nxt = n + s * gaps[si]          # move current cull position up the wheel
                si = si + 1                     # advance wheel index
                if si > 92159: si = 0
                while nxt in sieve:             # ensure each entry is unique by wheel
                    nxt += s * gaps[si]
                    si = si + 1                 # advance wheel index
                    if si > 92159: si = 0
                sieve[nxt] = (s, si)            # next non-marked multiple of a prime
            nni = ni + 1                        # advance wheel index
            if nni > 92159: nni = 0
            n += gaps[ni]; ni = nni             # advance on the wheel
    for p, pi in wheel_prime_pairs(): yield p   # strip out indexes

if __name__ == "__main__":
    main()
