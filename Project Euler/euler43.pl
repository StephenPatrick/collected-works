use warnings;
use strict;

my $sum = 0;
my $i = 1023456789; 
while ($i < 9876543211){
	#print $i."\n";
	if (substr ($i, 1, 3) % 2 == 0){
		if (substr ($i, 2, 3) % 3 == 0){
			if (substr ($i, 3, 3) % 5 == 0){
				if (substr ($i, 4, 3) % 7 == 0){
					if (substr ($i, 5, 3) % 11 == 0){
						if (substr ($i, 6, 3) % 13 == 0){
							if (substr ($i, 7, 3) % 17 == 0){
								if ($i =~ /^(?!.*([0-9]).*\1)[0-9]{10}$/){
									$sum += $i;
								}
								$i++;
							}
							else {$i += 1;}
						}
						else {$i += 10;}
					}
					else {$i += 100;}
				}
				else {$i += 1000;}
			}
			else {$i += 10000;}
		}
		else {$i += 100000;}
	}
	else {$i += 1000000;}
}
print $sum."\n";