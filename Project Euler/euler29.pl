use warnings;
use strict;
use v5.10;
use Time::HiRes;

my $time_a = Time::HiRes::time();sub Distinct_Powers {
	my $lower_lim = shift;
	my $upper_lim = shift;
	my @num_ray = ();
	for (my $i = $lower_lim;$i <= $upper_lim;$i++) {
		for (my $j = $lower_lim;$j <= $upper_lim;$j++) {
			my $x = $i ** $j;
			if (!($x ~~ @num_ray)) {
				push @num_ray, $x;			}
		}			}
	say scalar @num_ray;}
Distinct_Powers(2,100);
my $time_b = Time::HiRes::time();
my $complete_time = $time_b - $time_a;
say $complete_time;