def main():
	count = 0
	i = 0
	while True:
		h = hex(i)[2:]
		if len(h) > 16:
			break
		if "a" in h and "0" in h and "1" in h:
			print h
			count += 1
		i += 1

if __name__ == '__main__':
	main()