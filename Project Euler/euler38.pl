use warnings;
use strict;

my $str = "";
my $mult = 2;
my $factor;
for (my $i = 1; $i < 10000; $i++){
	#print "$i: ";
	$factor = $i;
	while (length $str < 9){
		$str .= $factor;
		$factor = $i * $mult;
		$mult++;
	}
	#print $str."\n";
	#if ($i == 192) {my $wait = <>;}
	if (length $str == 9){
		if (isPandigital($str)){
			print $str."\n";
			my $wait = <>;
		}
	}
	$mult = 2;
	$str = "";
}

sub isPandigital{
	my $i = shift;
	my $substr;
	my $no = 0;
	my $also_no = 1;
	for (my $j = 1; $j <= length $i; $j++){
		$no = 1;
		for (my $k = 0; $k < length $i; $k++){
			$substr = substr $i, $k, 1;
			if ($substr == $j){
				$no = 0;
				last;
			}
		}
		if ($no) {
			$also_no = 0;
			last;
		}
	}
	if ($also_no){
		return 1;
	}
	return 0;
}