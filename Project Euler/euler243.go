package main

import "fmt"
import "math"
import "math/big"

type memoizeFunction func(int) interface{}

var mem_findDivisors memoizeFunction

func Memoize(function memoizeFunction) memoizeFunction {
	cache := make(map[string]interface{})
	return func(x int, xs ...int) interface{} {
		key := fmt.Sprint(x)
		for _, i := range xs {
			key += fmt.Sprintf(",%d", i)
		}
		if val, found := cache[key]; found {
			fmt.Println("Cached", val)
			return val
		}
		val := function(x, xs...)
		cache[key] = val
		return val
	}
}

// Didn't write this, have written this function too many times
// in too many languages to bother again
func findDivisors(num int) []int {
	divisors := []int{}
	// Speed optimization: Find all factors until root
	for i := 1; i <= int(math.Sqrt(float64(num)))+1; i++ {
		if num%i == 0 {
			divisors = append(divisors, i)
		}
	}
	// Then calculate complimentary factors and add them.
	hdiv := []int{}
	for _, v := range divisors {
		if (num / v) != v {
			hdiv = append(hdiv, (num / v))
		}
	}
	for _, v := range hdiv {
		divisors = append(divisors, v)
	}
	// And clean up so we only have unique factors.
	return removeDuplicates(divisors)
}

func removeDuplicates(a []int) []int {
	result := []int{}
	seen := map[int]int{}
	for _, val := range a {
		if _, ok := seen[val]; !ok {
			result = append(result, val)
			seen[val] = val
		}
	}
	return result
}

func main() {
	for d := 2; true; d++ {
		for n := 0; n < d; n++ {
			n_divisors := findDivisors(n)
			d_divisors := findDivisors(d)
		}
	}
}
