from time import clock
import sys
from copy import copy, deepcopy

matrix = [
	[  7,  53, 183, 439, 863, 497, 383, 563,  79, 973, 287,  63, 343, 169, 583],
	[627, 343, 773, 959, 943, 767, 473, 103, 699, 303, 957, 703, 583, 639, 913],
	[447, 283, 463,  29,  23, 487, 463, 993, 119, 883, 327, 493, 423, 159, 743],
	[217, 623,   3, 399, 853, 407, 103, 983,  89, 463, 290, 516, 212, 462, 350],
	[960, 376, 682, 962, 300, 780, 486, 502, 912, 800, 250, 346, 172, 812, 350],
	[870, 456, 192, 162, 593, 473, 915,  45, 989, 873, 823, 965, 425, 329, 803],
	[973, 965, 905, 919, 133, 673, 665, 235, 509, 613, 673, 815, 165, 992, 326],
	[322, 148, 972, 962, 286, 255, 941, 541, 265, 323, 925, 281, 601,  95, 973],
	[445, 721,  11, 525, 473,  65, 511, 164, 138, 672,  18, 428, 154, 448, 848],
	[414, 456, 310, 312, 798, 104, 566, 520, 302, 248, 694, 976, 430, 392, 198],
	[184, 829, 373, 181, 631, 101, 969, 613, 840, 740, 778, 458, 284, 760, 390],
	[821, 461, 843, 513,  17, 901, 711, 993, 293, 157, 274,  94, 192, 156, 574],
	[ 34, 124,   4, 878, 450, 476, 712, 914, 838, 669, 875, 299, 823, 329, 699],
	[815, 559, 813, 459, 522, 788, 168, 586, 966, 232, 308, 833, 251, 631, 107],
	[813, 883, 451, 509, 615,  77, 281, 613, 459, 205, 380, 274, 302,  35, 805]]

memo = {}

# Takes a sorted -with original index- matrix
def maxMatrixSum(m, bestSoFar, places, prefix):
	str_m = str(places)
	if str_m not in memo:
		invalid = -1
		indexes = {}

		total = 0

		# Check the validity of the matrix
		for i in xrange(len(m)):
			sub = m[i][places[i]]
			total += sub[0]
			if sub[1] not in indexes:
				indexes[sub[1]] = [i]
			else:
				invalid = sub[1]
				indexes[sub[1]].append(i)

		if total <= bestSoFar:
			memo[str_m] = 0
			return 0

		if invalid != -1:
			bestNew = 0
			for i in indexes[invalid]:
				#if len(prefix) < 20:
					#print prefix,"Next"
				if places[i] == (len(m[0]) - 1):
					continue
				places[i] += 1
				nextNew = maxMatrixSum(m, bestNew, copy(places), prefix + "-")
				if nextNew > bestNew:
					bestNew = nextNew
				places[i] -= 1
			memo[str_m] = bestNew
			return bestNew

		#print prefix, "total:", total
		#print "invalid:", invalid

		memo[str_m] = total
	#else:
		#print prefix, str_m

	return memo[str_m]


def sortWithIndex(w,y):
	a = matrix[y][0:w]
	a2 = copy(a)
	a2.sort()
	a2.reverse()
	outArray = []
	for i in a2:
		index = a.index(i)
		outArray.append([i, index])
	return outArray

def main(w,h):
	sortedIndex = []
	for i in xrange(h):
		sortedIndex.append(sortWithIndex(w,i))
	for l in sortedIndex:
		print l
	places = [0] * h
	print ""
	out = maxMatrixSum(sortedIndex, 0, places, "")
	print out
	for k in memo:
		if memo[k] == out:
			print k

if __name__ == '__main__':
	s = clock()
	main(int(sys.argv[1]), int(sys.argv[2]))
	print (clock() - s)