package main

import "fmt"
import "math/big"

// Send the sequence 2, 3, 4, ... to channel 'ch'.
func Generate(ch chan<- int) {
	for i := 2; ; i++ {
		ch <- i // Send 'i' to channel 'ch'.
	}
}

// Copy the values from channel 'in' to channel 'out',
// removing those divisible by 'prime'.
func Filter(in <-chan int, out chan<- int, prime int) {
	for {
		i := <-in // Receive value from 'in'.
		if i%prime != 0 {
			out <- i // Send 'i' to 'out'.
		}
	}
}

func main() {

	// First set of iterating primes
	ch := make(chan int) // Create a new channel.
	go Generate(ch)      // Launch Generate goroutine.
	// Second set (for subtraction)
	subch := make(chan int) // Create a new channel.
	go Generate(subch)      // Launch Generate goroutine.

	primesums := []int64{}
	primesums = append(primesums, 2)

	//length := 0;
	max_length := 0
	prime := <-ch
	ch1 := make(chan int)
	go Filter(ch, ch1, prime)
	ch = ch1
	for i := 1; i < 700; i++ {
		prime = <-ch

		primesums = append(primesums, (int64(prime) + primesums[i-1]))

		ch1 = make(chan int)
		go Filter(ch, ch1, prime)
		ch = ch1
	}
	fmt.Println(primesums)
	for i, sum1 := range primesums {
		for j, sum2 := range primesums {
			val := sum2 - sum1
			if big.NewInt(val).ProbablyPrime(10) && val < 1000000 {
				if j-i > max_length {
					max_length = j - i
					fmt.Println(val, max_length)
				}
			}
		}
	}
}
