use warnings;
use strict;

my $result = 0;
my $running = 1;
my $i = 1;
my $n;
my $m;
while ($running){
	$n = $i * ((3 * $i) - 1) / 2;
	for (my $j = $i - 1; $j > 0; $j--){
		$m = $j * ((3 * $j) - 1) / 2;
		if (isPentagonal($n - $m) && isPentagonal($n + $m)){
			print (($n-$m)."\n");
			$running = 0;
			last;
		}
	}
	$i++
}

sub isPentagonal{
	my $n = shift;
	return (((sqrt((24*$n) +1) +1) / 6) == int(((sqrt((24*$n) +1) +1) / 6)));
}
