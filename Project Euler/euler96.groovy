// I commented this one because its big and I like it

/***
*
* "Main".
* Reads the input file and peforms the final summation.
* 
*/
euler96 = {
    
    file = new File("euler96-input.txt")
    grids = []
    int grid_index = -1
    file.eachLine {line->
        if (line[0] == "G") {
            grids.add([])
            grid_index++
        } else {
            grids[grid_index].add(line.toCharArray().collect{it.toString()})    
        }
    }
    
    solved = []
    for (grid in grids) {
        solved.add(solve(grid))
    }
    
    // sum top left corners
    int sum = 0
    for (grid in solved) {
        sum += (grid[0][0]+grid[0][1]+grid[0][2]).toInteger()
    }
    println sum
}

/**
* Solve a sudoku grid.
* Uses an iterative approach, 
* To make backtracking easier to follow.
*/
solve = {grid->
    
    int empty_spaces = 81
    
    // Initial grid
    moves_avail = []
    a = ["1","2","3","4","5","6","7","8","9"]
    for (y = 0; y < 9; y++) {
        moves_avail.add([])
        for (x = 0; x < 9; x++) {
            if (grid[y][x] == "0") {             
                moves_avail[y].add(a.clone())
            } else {
                empty_spaces--
                moves_avail[y].add([])
            }
        }
    }
    
    // Analyze initial grid
    for (int y = 0; y < 9; y++) {
        for (int x = 0; x < 9; x++) {
            if (grid[y][x] != "0") {
                updatePossibilities(moves_avail,x,y,grid[y][x])
            }
        }
    }
    
    
    guessStorage = []
    guessesMade = []
    
    // Evaluate empty squares not in initial grid
    while (empty_spaces > 0) {
        
        // If there's a square with no remaining options to fill with,
        // we're very unhappy and we need to revert our last guess.
        // If we haven't made a guess, the sudoku puzzle is 
        // unsolvable and we fall over. 
        veryunhappy = findUnfillableSquare(grid, moves_avail)
        if (veryunhappy) {
            // Revert to before our last guess
            (grid, moves_avail, empty_spaces) = guessStorage.pop()
            (badX, badY, val) = guessesMade.pop()
            // Remove the bad guess from our options
            moves_avail[badY][badX].removeElement(val)
        }
        
        // Make sure we're happy, that we have a space where a particular
        // number must go. 
        happy = fillSolePossibility(grid, moves_avail)
        if (!happy) {
            // If we aren't happy, then we gotta make a guess. 
            // Store the grid, make the guess, cross fingers
            guessStorage.add([deepcopy(grid),deepcopy(moves_avail),empty_spaces])
            guessesMade.add(makeBestGuess(grid, moves_avail))
        }
        // Whether we guessed or knew, there is one less empty space.
        empty_spaces--
    }
    
//    for (row in grid) {
//        println row
//    }
//    println " "
    return grid
}

/**
* Create a non-shallow copy of a collection.
* I didn't write this. I forgot who did. uhh
*/
deepcopy = {orig->
     bos = new ByteArrayOutputStream()
     oos = new ObjectOutputStream(bos)
     oos.writeObject(orig); oos.flush()
     bin = new ByteArrayInputStream(bos.toByteArray())
     ois = new ObjectInputStream(bin)
     return ois.readObject()
}

/**
* Remove all instances of the given value
* from possibility lists which can't
* share that value with [y,x]
*/
updatePossibilities = {moves,x,y,val->
    
    // Same column
    for (y2 = 0; y2 < 9; y2++) {
        moves[y2][x].removeElement(val)
    } 
    // Same row
    for (x2 = 0; x2 < 9; x2++) {
        moves[y][x2].removeElement(val)
    }
    // The 3x3 section we exist in
    // integer division, groovy? please?
    for (int qx = Math.floor(x/3)*3; qx < (Math.floor(x/3)*3)+3; qx++) {
        for (int qy = Math.floor(y/3)*3; qy < (Math.floor(y/3)*3)+3; qy++) {
            moves[qy][qx].removeElement(val)
        }
    }
}

/**
* Place a number in the grid and perform required follow up
*/
placeNumber = {grid,moves,x,y,val->
    grid[y][x] = val
    moves[y][x] = []
    updatePossibilities(moves,x,y,val)
}

// Todo:
// All of the following three functions could maybe
// happen at the same time????????

/**
* Find a location with only one possible number
* to be filled with, and fill it.
*/
fillSolePossibility = {grid,moves->
    
    for (y = 0; y < 9; y++) {
        for (x = 0; x < 9; x++) {
            if (grid[y][x] == "0" && moves[y][x].size() == 1) {
                placeNumber(grid,moves,x,y,moves[y][x][0])
                return true
            }
        }
    }
    return false
}

/**
* Find a location with no available numbers 
* to be filled with. 
*/
findUnfillableSquare = {grid,moves->
    
    for (y = 0; y < 9; y++) {
        for (x = 0; x < 9; x++) {
            if (grid[y][x] == "0" && moves[y][x].size() == 0) return true
        }
    }
    return false
}

/**
* Find the first location in the grid with the lowest number of elements,
* which isn't 1, because this should be called after failing to find such a location.
*/
makeBestGuess = {grid,moves-> 
    
    int bestLen = 10
    int bestX = -1
    int bestY = -1
    for (y = 0; y < 9; y++) {
        for (x = 0; x < 9; x++) {
            if (grid[y][x] == "0" && moves[y][x].size() < bestLen) {
                bestX = x
                bestY = y
                bestLen = moves[y][x].size()
            }
        }
    }
    placeNumber(grid,moves,bestX,bestY,moves[bestY][bestX][0])
    return [bestX,bestY,grid[bestY][bestX]]
}

euler96()