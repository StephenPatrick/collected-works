package main

import "fmt"
import "os"
import "bufio"
import "log"
import "strings"
import "strconv"

func g_atoi(s string) int {
	i, err := strconv.Atoi(s)
	if err != nil {
		log.Fatal(err)
	}
	return i
}

func main() {

	file, err := os.Open("euler59-input.txt") // For read access.
	if err != nil {
		log.Fatal(err)
	}

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		parts := strings.FieldsFunc(scanner.Text(), func(c rune) bool { return c == ',' })
		l := len(parts)
		byte_parts := make([]byte, l)
		for i := 0; i < l; i++ {
			byte_parts[i] = byte(g_atoi(parts[i]))
		}
		quit := -1
		// 97 through 122 are lower-case letters
		for i := 97; i <= 122; i++ {
			for j := 97; j <= 122; j++ {
				for k := 97; k <= 122; k++ {
					key := [...]byte{
						byte(i),
						byte(j),
						byte(k)}
					m := 0
					b := make([]byte, l)
					quit = -1
					for n := 0; n < l; n++ {
						b[n] = byte_parts[n] ^ key[m]
						// Break on garbage characters
						// like {}~|$%#&, control characters
						if (b[n] < 39 && (b[n] != 32 && b[n] != 33 && b[n] != 10 && b[n] != 9)) ||
							(b[n] > 122) {
							quit = m // In case we wanted more sophisticated usage, incrementing
							// the bad character of i,j,k.
							break
						}
						m = (m + 1) % 3
					}
					if quit != -1 {
						continue
					}
					fmt.Println(string(b))
					fmt.Println(key)
					sum := 0
					for n := 0; n < l; n++ {
						sum += int(b[n])
					}
					fmt.Println(sum)
				}
			}
		}
	}
}
