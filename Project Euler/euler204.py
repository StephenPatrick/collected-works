from primefac import gen_hamming, prime_generator
import sys
from time import clock

# def main(limit, h):
	
# 	# Brute force approach
# 	total = 0
# 	for i in xrange(1,limit):
# 		if gen_hamming(i,h):
# 			total += 1
# 			#print i
# 	print total

def main(limit,h):

	# Prime iteration approach
	# This finds all non-hamming numbers.
	# a much faster approach would find all
	# hamming numbers, recursively,
	# through finding all numbers which are 
	# exclusively multiples of primes below 100
	# (whose product is below the given limit)
	hamming_array = [True] * (limit+1)
	hamming_array[0] = False
	p = prime_generator()
	n = next(p)
	while n <= h:
		n = next(p)
	while n < limit:
		i = n
		for i in xrange(n,(limit+1),n):
			hamming_array[i] = False
		n = next(p)
	print sum(hamming_array)

if __name__ == '__main__':
	s = clock()
	main(int(sys.argv[1]), int(sys.argv[2]))
	print (clock() - s)