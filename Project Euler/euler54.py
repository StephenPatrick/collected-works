st_flush = 0
st = 0
foak = 0
full_house = 0

def main():
  hands = open('poker.txt', 'r')
  reading = True
  sep = ", "
  a_wins = 0
  b_wins = 0
  for the_board in hands:
    if (the_board == ''):
      reading = False
      break
    cards_a = the_board[:14].split()
    cards_b = the_board[15:].split()
    value_a = hand(the_board[:14])
    value_b = hand(the_board[15:])
    print value_a
    print sep.join(cards_a)
    print value_b
    print sep.join(cards_b)
    #wait = raw_input()
    if value_a > value_b:
      a_wins += 1
    elif value_b > value_a:
      b_wins += 1
    else:
      winner = raw_input("Who won? ")
      if winner == "a":
        a_wins += 1
      else:
        b_wins += 1
  print st_flush
  print st
  print foak
  print full_house
  print "A won %d times." % (a_wins)
  
def hand(cards):
  cards = cards.split()
  hand_value = 0
  high_card = 0
  strvalues = []
  suits = []
  for card in cards:
    strvalues.append(card[0])
    suits.append(card[1])

  values = []
  low_ace = []
  for value in strvalues:
    if value == "T":
      values.append(10)
      low_ace.append(10)
    elif value == "J":
      values.append(11)
      low_ace.append(11)
    elif value == "Q":
      values.append(12)
      low_ace.append(12)
    elif value == "K":
      values.append(13)
      low_ace.append(13)
    elif value == "A":
      values.append(14)
      low_ace.append(1)
    else:  
      values.append(int(value))
      low_ace.append(int(value))

  high_card = max(values)
  hand_value += high_card
    
  prev_suit = suits[1]
  flush = 0
  for suit in suits:
    if suit == prev_suit:
      flush = 1
    else:
      flush = 0
      break
    
  straight = 0
  cont = 0
  sorted_low = sorted(low_ace)
  sorted_val = sorted(values)
  for i in range(4):
    if sorted_val[i+1] == sorted_val[i] + 1:
      straight = 1
    else:
      straight = 0
      break
  if straight == 0:
    for i in range(4):
      if low_ace[i+1] == low_ace[i] + 1:
        straight = 1
      else:
        straight = 0
        break

  #####
  #Straight Flush?
  if straight == 1 and flush == 1:
    hand_value += 10000000
    st_flush = 1
    return hand_value

  #####
  #Four of a kind?
  counts = []
  for check_val in values:
    count = 0
    for value in values:
      if check_val == value:
        count += 1
    counts.append(count)
  for i in range(5):
    if counts[i] == 4:
      hand_value += 9000000
      hand_value += values[i] * 10000
      foak = 1
      return hand_value

  #####
  #Full House?
  trips = -1
  for i in range(5):
    if counts[i] == 3:
      trips = i
      break
  doubs_1 = -1
  doubs_2 = -1
  for i in range(5):
    if counts[i] == 2:
      if doubs_1 == -1:
        doubs_1 = i
      elif values[i] != values[doubs_1]:
        doubs_2 = i
  if trips != -1 and doubs_1 != -1:
    hand_value += 8000000
    hand_value += values[trips] * 10000
    hand_value += values[doubs_1] * 100
    full_house = 1

  #####
  #Flush?
  elif flush:
    hand_value += 7000000

  #####
  #Straight?
  elif straight:
    hand_value += 6000000
    st = 1

  #####
  #Trips?
  elif trips != -1:
    hand_value += 5000000
    hand_value += values[trips] * 10000

  #####
  #Two pair?
  elif doubs_1 != -1 and doubs_2 != -1:
    hand_value += 4000000
    high_doubs = max([values[doubs_1],values[doubs_2]])
    low_doubs = min([values[doubs_1],values[doubs_2]])
    hand_value += high_doubs * 10000
    hand_value += low_doubs * 100

  #####
  #Pair?
  elif doubs_1 != -1:
    hand_value += 3000000
    hand_value += values[doubs_1] * 10000
    
  return hand_value

if __name__ == "__main__":
    main()
