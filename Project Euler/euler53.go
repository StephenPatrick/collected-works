package main

import "fmt"
import "math/big"
import "time"

var factorials map[int64]*big.Int

func timeTrack(start time.Time, name string) {
	elapsed := time.Since(start)
	fmt.Printf("%s took %s", name, elapsed)
}

func comb_int(a *big.Int, b *big.Int) *big.Int {

	acopy := big.NewInt(0).Set(a)
	acopy2 := big.NewInt(0).Set(a)
	bcopy := big.NewInt(0).Set(b)
	bcopy2 := big.NewInt(0).Set(b)

	aminusb := acopy2.Sub(acopy2, bcopy2)

	fa := factorial_int(acopy)
	fb := factorial_int(bcopy)
	fab := factorial_int(aminusb)

	return fa.Div(fa, (fb.Mul(fb, fab)))
}

func factorial_int(n *big.Int) *big.Int {
	if factorials[n.Int64()] != nil {
		return factorials[n.Int64()]
	}
	if n.Cmp(big.NewInt(0)) == 0 {
		return big.NewInt(1)
	}
	sub1 := big.NewInt(0).Set(n)
	sub1 = sub1.Sub(sub1, big.NewInt(1))
	val := n.Mul(n, factorial_int(sub1))
	factorials[n.Int64()] = val
	return val
}

func main() {
	defer timeTrack(time.Now(), "euler53")

	factorials = make(map[int64]*big.Int)

	factorial_int(big.NewInt(100))
	i := big.NewInt(23)
	amillion := big.NewInt(1000000)
	ahundred := big.NewInt(100)
	total := 0
	for i.Cmp(ahundred) < 1 {
		j := big.NewInt(1)
		for true {
			if j.Cmp(i) == 1 {
				break
			}
			val := comb_int(i, j)
			if val.Cmp(amillion) == 1 {
				total++
			}
			j.Add(j, big.NewInt(1))
		}
		i.Add(i, big.NewInt(1))
	}
	fmt.Println(total)
}
