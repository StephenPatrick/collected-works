use warnings;
use strict;

use v5.12;
my @computed;
my $file;
open $file, "triangle.txt" or die "NOOO!"; 
my $running = 1;
my $data;
my $n;
my @line;
my @grid = ();
while (<$file>){
	@line = split;
	push @grid, [ @line ];
}
#my @grid = (
#	[75],
#	[95, 64],
#	[17, 47, 82],
#	[18, 35, 87, 10],
#	[20, 4, 82, 47, 65],
#	[19, 1, 23, 75, 3, 34],
#	[88, 2, 77, 73, 7, 63, 67],
#	[99, 65, 4, 28, 6, 16, 70, 92],
#	[41, 41, 26, 56, 83, 40, 80, 70, 33],
#	[41, 48, 72, 33, 47, 32, 37, 16, 94, 29],
#	[53, 71, 44, 65, 25, 43, 91, 52, 97, 51, 14],
#	[70, 11, 33, 28, 77, 73, 17, 78, 39, 68, 17, 57],
#	[91, 71, 52, 38, 17, 14, 91, 43, 58, 50, 27, 29, 48],
#	[63, 66, 4, 68, 89, 53, 67, 30, 73, 16, 69, 87, 40, 31],
#	[4, 62, 98, 27, 23, 9, 70, 98, 73, 93, 38, 53, 60, 4, 23],
#);

my $value = tri_grid_max(0,0);
say $value;


sub tri_grid_max{
	my $y = shift;
	my $x = shift;
	return $computed[$y][$x] if defined $computed[$y][$x];
	return $computed[$y][$x] = $grid[$y][$x] if $y == 99;
	my $l_max = tri_grid_max($y+1,$x);
	my $r_max = tri_grid_max($y+1,$x+1);
	$computed[$y][$x] = $grid[$y][$x] + int_max($l_max,$r_max);
}

sub int_max{
	my $y = shift;
	my $x = shift;
	if ($x > $y) {
		return $x;
	}
	else {return $y;}
}
