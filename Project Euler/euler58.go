package main

import "fmt"
import "math/big"

func main() {

	primes := 3
	not_primes := 2
	length := big.NewInt(4)
	val := big.NewInt(9)
	two := big.NewInt(2)

	for float64(primes)/float64(not_primes+primes) >= 0.10 {
		for i := 0; i < 4; i++ {
			val.Add(val, length)
			if val.ProbablyPrime(2) {
				primes += 1
			} else {
				not_primes += 1
			}
		}
		length.Add(length, two)
		fmt.Println(primes, not_primes)
		fmt.Println(length, float64(primes)/float64(not_primes+primes))
	}

	fmt.Println(length.Add(length, big.NewInt(1)))
}
