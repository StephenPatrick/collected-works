package main

import "fmt"

//import "os";
//import "bufio";
import "log"

//import "strings";
import "strconv"

func g_atoi(s string) int {
	i, err := strconv.Atoi(s)
	if err != nil {
		log.Fatal(err)
	}
	return i
}

func remove_node(s string) {
	delete(path_map, s)
	// Remove this node from the rest of the mapping
	for key, val := range path_map {
		for k := 0; k < len(val[0]); k++ {
			if val[0][k] == s {
				val[0] = append(val[0][:k], val[0][k+1:]...)
			}
		}
		for k := 0; k < len(val[1]); k++ {
			if val[1][k] == s {
				val[1] = append(val[1][:k], val[1][k+1:]...)
			}
		}
		if len(val[0]) == 0 || len(val[1]) == 0 {
			remove_node(key)
		} else {
			path_map[key] = val
		}
	}
}

var path_map = make(map[string][2][]string)

func populate(fn func(int) int) []string {
	arr := []string{}
	for i := 0; true; i++ {
		val := fn(i)
		if val < 1000 {
		} else if val > 9999 {
			break
		} else {
			arr = append(arr, strconv.Itoa(val))
		}
	}
	return arr
}

func main() {

	// generate triangle, square, pentagonal, etc, numbers
	triangles := populate(func(i int) int { return i * (i + 1) / 2 })
	squares := populate(func(i int) int { return i * i })
	pentagons := populate(func(i int) int { return i * ((3 * i) - 1) / 2 })
	hexagons := populate(func(i int) int { return i * ((2 * i) - 1) })
	septagons := populate(func(i int) int { return i * ((5 * i) - 3) / 2 })
	octagons := populate(func(i int) int { return i * ((3 * i) - 2) })

	n_arys := [6][]string{octagons, septagons, hexagons, pentagons, squares, triangles}
	// loop through <each set of numbers>
	for j := 0; j < len(n_arys); j++ {
		for i := 0; i < len(n_arys[j]); {
			number := n_arys[j][i]
			front := number[0:2]
			back := number[2:4]
			m_number := strconv.Itoa(j) + ":" + number
			path_map[m_number] = [2][]string{}
			for k := 0; k < len(n_arys); k++ {
				if k == j {
					continue
				}
				for m := 0; m < len(n_arys[k]); m++ {
					check := n_arys[k][m]
					if check[2:4] == front {
						mapping := path_map[m_number]
						mapping[0] = append(mapping[0], strconv.Itoa(k)+":"+check)
						path_map[m_number] = mapping
					}
					if check[0:2] == back {
						mapping := path_map[m_number]
						mapping[1] = append(mapping[1], strconv.Itoa(k)+":"+check)
						path_map[m_number] = mapping
					}
				}
			}
			if len(path_map[m_number][0]) == 0 ||
				len(path_map[m_number][1]) == 0 {
				// remove this element from this slice
				n_arys[j] = append(n_arys[j][:i], n_arys[j][i+1:]...)
				// Remove this node's root
				remove_node(m_number)
			} else {
				i++
			}
		}
	}
	// Dumb 6-depth DFS
	for key0, elem_list := range path_map {
		for _, elem1 := range elem_list[1] {
			if elem1[0] == key0[0] {
				continue
			}
			for _, elem2 := range path_map[elem1][1] {
				if elem2[0] == key0[0] || elem1[0] == elem2[0] {
					continue
				}
				for _, elem3 := range path_map[elem2][1] {
					if elem3[0] == key0[0] || elem3[0] == elem2[0] || elem3[0] == elem1[0] {
						continue
					}
					for _, elem4 := range path_map[elem3][1] {
						if elem4[0] == key0[0] || elem4[0] == elem3[0] || elem4[0] == elem2[0] || elem4[0] == elem1[0] {
							continue
						}
						for _, elem5 := range path_map[elem4][1] {
							if elem5[0] == key0[0] || elem5[0] == elem4[0] || elem5[0] == elem3[0] || elem5[0] == elem2[0] || elem5[0] == elem1[0] {
								continue
							}
							for _, elem6 := range path_map[elem5][1] {
								if elem6 == key0 {
									fmt.Println(key0, elem1, elem2, elem3, elem4, elem5)
									return
								}
							}
						}
					}
				}
			}
		}
	}
}
