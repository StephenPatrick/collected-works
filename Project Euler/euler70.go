package main

import "fmt"

import "strconv"
import "sort"

// Send the sequence 2, 3, 4, ...
func Generate(ch chan<- int) {
	for i := 2; ; i++ {
		ch <- i
	}
}

// Copy the values from channel in to channel out,
// removing those divisible by prime.
func Filter(in <-chan int, out chan<- int, prime int) {
	for {
		i := <-in
		if i%prime != 0 {
			out <- i
		}
	}
}

func Next_Prime(ch <-chan int) (prime int, new_ch chan int) {
	prime = <-ch
	new_ch = make(chan int)
	go Filter(ch, new_ch, prime)
	return
}

func main() {

	limit := 10000000

	primes := []int{}
	ch := make(chan int)
	a := 0
	go Generate(ch)
	for a <= 10000 {
		fmt.Println(a)
		// Get the next prime, update
		// our prime generation channel.
		a, ch = Next_Prime(ch)
		primes = append(primes, a)
	}
	max := float64(10000)
	max_i := 0
	for i := 2; i < limit; i++ {
		n := i
		factors := []int{}
		for _, prime := range primes {
			if prime > n {
				break
			}
			if n%prime == 0 {
				factors = append(factors, prime)
				for n%prime == 0 {
					n = n / prime
				}
			}
		}
		if len(factors) == 0 {
			factors = []int{n}
		}
		t := φ(i, factors)
		if isPermutation(i, t) {
			if float64(i)/float64(t) < max {
				max = float64(i) / float64(t)
				max_i = i
			}
		}
	}

	fmt.Println(max, max_i)
}

func φ(n int, factors []int) (totient float64) {
	totient = float64(n)
	for _, factor := range factors {
		fl := float64(factor)
		totient *= ((fl - 1) / fl)
	}
	return
}

func isPermutation(x1 int, x2 float64) bool {
	s1 := strconv.Itoa(x1)
	s2 := strconv.Itoa(int(x2))
	a1 := toStringArray(s1)
	a2 := toStringArray(s2)
	sort.Strings(a1)
	sort.Strings(a2)
	return (testEq(a1, a2))
}

func toStringArray(s string) []string {
	a := []string{}
	for _, c := range s {
		a = append(a, string(c))
	}
	return a
}

// http://stackoverflow.com/questions/15311969/checking-the-equality-of-two-slices
func testEq(a, b []string) bool {

	if a == nil && b == nil {
		return true
	}

	if a == nil || b == nil {
		return false
	}

	if len(a) != len(b) {
		return false
	}

	for i := range a {
		if a[i] != b[i] {
			return false
		}
	}

	return true
}
