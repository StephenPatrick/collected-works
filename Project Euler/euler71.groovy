sternB={minN,minD,maxN,maxD,d->
    int XN = minN + maxN
    int XD = minD + maxD
    !d--?[[minN,minD]]:sternB(minN,minD,XN,XD,d)+sternB(XN,XD,maxN,maxD,d)
}

x={->
    long r = 0
    long s = 1
    for (int i = 1000000; i > 2; i--) {
        long p = (3*i-1) / 7
        if (p * s > r * i) {
            s = i
            r = p
        }
    }
    println(s + " " + r)
}
x()