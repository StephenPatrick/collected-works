import sys
from time import clock

def isPalindrome(n):
	s = str(n)
	for i in xrange((len(s)/2)):
		if s[i] != s[-1*(i+1)]:
			return False
	return True


def main(limit):
	start = clock()
	d = {}
	val = 5
	nextStart = 2
	nextAdd = 3
	while True:
		if nextAdd > nextStart and isPalindrome(val):
			if not val in d:
				d[val] = True
		val += nextAdd*nextAdd
		nextAdd += 1
		if val > limit:
			val = nextStart*nextStart
			if val > limit:
				break
			nextStart += 1
			nextAdd = nextStart
	print sum(d.keys())
	print (clock() - start)


if __name__ == '__main__':
	main(int(sys.argv[1]))