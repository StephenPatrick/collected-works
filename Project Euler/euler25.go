package main

import "fmt"
import "math/big"

func fib() func() *big.Int {
	a, b := big.NewInt(0), big.NewInt(1)
	return func() *big.Int {
		a, b = b, a.Add(a, b)
		return a
	}
}

func main() {
	f := fib()
	i := 1
	for true {
		l := len(f().String())
		fmt.Println(l)
		if l == 1000 {
			break
		}
		i++
	}
	fmt.Println(i)
}
