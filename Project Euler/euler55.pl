use warnings;
use strict;
use bignum;
use v5.10;

sub IntReverse {
	my $x = shift;
	my $new_x = 0;
	for (my $i=0;$i<length substr ($x,0);$i++) {
		my $change_value = substr($x,$i,1) * (10 ** $i);		$new_x += $change_value;
	}
	return $new_x;
}

sub Lychrel_Test {
	my $origx = shift;
	my $x = $origx;	my $y = IntReverse($x);
	for (my $i=0;$i<50;$i++) {
		$x += $y;
		$y = IntReverse($x);
		if ($y == $x) {return 0}	}
	say "$origx is a Lychrel number.";
	return 1;}
my $count = 0;for (my $i=5;$i<10001;$i++) {
	$count+= Lychrel_Test($i);}
say "Final count: $count";