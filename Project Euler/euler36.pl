use warnings;
use strict;

my $sum = 0;
for (my $i = 1; $i <= 1000000; $i++){
	if (isPalindrome($i)){
		print $i."\n";
		if (isPalindrome(DECtoBIN($i))){
			$sum += $i;
		}
	}
}
print $sum."\n";

sub isPalindrome{
	my $n = shift;
	my $rev = scalar reverse $n;
	return ($n == $rev);
}

sub DECtoBIN{
	my $n = shift;
	my $bin = "";
	while ($n > 0){
		$bin .= $n % 2;
		$n = int($n / 2);
	}
	return scalar reverse $bin;
}