package main

import "fmt"
import "math/big"
import "strings"
import "sort"
import "bytes"

// Sorts characters of a string
func sortString(w string) string {
	s := strings.Split(w, "")
	sort.Strings(s)
	return str_append(s...)
}

var buffer bytes.Buffer

func str_append(strs ...string) string {
	buffer.Reset()
	for _, str := range strs {
		buffer.WriteString(str)
	}
	return buffer.String()
}

func fuellen(fn func(int) string) string {
	ansam := map[string]int{}
	for i := 1; true; i++ {
		wert := sortString(fn(i))
		if val, ok := ansam[wert]; ok {
			ansam[wert] = val + 1
			if ansam[wert] == 5 {
				return fn(i)
			}
		} else {
			ansam[wert] = 1
		}
	}
	return "err"
}

func main() {
	antwort := fuellen(func(x int) string {
		y := big.NewInt(int64(x))
		return y.Exp(y, big.NewInt(int64(3)), nil).String()
	})
	fmt.Println(antwort)
}
