from __future__ import division
import sys
from time import clock

def is_bouncy(n):
	s = str(n)
	prev = ord(s[0])
	increasing = False
	decreasing = False
	for c in s:
		if ord(c) > prev:
			increasing = True
		elif ord(c) < prev:
			decreasing = True 
		prev = ord(c)
	return increasing and decreasing


def frac_to_precent(num,denom):
	return 100 * (num / denom)

def main(limit):
	bouncy = 0
	not_bouncy = 99
	i = 99
	percent = frac_to_precent(bouncy,not_bouncy+bouncy)

	while percent < limit:
		i += 1
		if is_bouncy(i):
			bouncy += 1
		else:
			not_bouncy += 1
		percent = frac_to_precent(bouncy,not_bouncy+bouncy)

	print i


if __name__ == '__main__':
	main(int(sys.argv[1]))