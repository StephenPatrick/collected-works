#Counting Rectangles
use warnings;
use strict;
use bigint;
use Memoize;
no warnings 'recursion';

sub rect_count{
	my ($x, $y) = @_;
	if ($y == 1) { return ($x*($x +1)) / 2;}
	return rect_count($x,$y-1) + $y*(($x*($x +1)) / 2);
}
memoize('rect_count');
my @closest = ();
my $val = 2000000;
my $x;
my $i;
my $j;
for ($i = 1; $i < 100; $i++){
	for ($j = $i; $j < 100; $j++){
		$x = rect_count($i,$j);
		if (abs(2000000 - $x) < $val){
			@closest = ($i, $j);
			$val = abs(2000000 - $x);
		}
	}
}
foreach (@closest) {print; print "\n";}