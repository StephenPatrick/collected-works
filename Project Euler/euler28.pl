use warnings;
use strict;
sub Spiral_Diagonal {
	my $max = shift;
	my $start = shift;
	my $increment = shift;
	my $sum = shift;
	$sum += $increment * 10;
	$sum += $start * 4;
	$start = $start + ($increment * 4);
	$increment += 2;
	if ($start == $max) {print $sum;}
	else {Spiral_Diagonal($max,$start,$increment,$sum)}}
Spiral_Diagonal(1002001,1,2,1);