import groovy.transform.TailRecursive

@TailRecursive
def fact(x, tot=1) {
    if (x == 0) return tot
    return fact(x-1, x * tot)
}

factDigits = {x->
    sum = 0
    s = Integer.toString(x)
    for (j = 0; j < s.length(); j++){
        y = Integer.parseInt(s[j])
        sum += fact(y)
    }
    return sum
}

euler74 = {-> 
    numbermap = [:]
    numbermap[1] = 0
    numbermap[2] = 0
    numbermap[145] = 0
    numbermap[169] = 2
    numbermap[871] = 1
    numbermap[872] = 1
    result = 0
    for (i = 1; i < 1000000; i++) {

        if (i % 1000 == 0) {
            println i
        }
        if (numbermap.containsKey(i)) {
            continue
        }

        int cur = i
        visited = [cur]
        int length = 0
        while (length == 0){
            next = factDigits(cur)
            if (next == cur) {
                numbermap[cur] = 0
            }
            cur = next
            if (numbermap.containsKey(cur)) {
                length = numbermap[cur] + 1
            } else {
                visited.add(cur)
            }
        }
        for (j = visited.size()-1; j >=0; j--) {
            numbermap[visited[j]] = length
            length++
        }
    }
    return numbermap
}


m = euler74()