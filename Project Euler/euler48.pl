# This can be made faster using a recursive exponentiation method
use warnings;
use strict;
use bigint;

my $sum = 0;
for (my $i = 1; $i < 1001; $i++){
	print $i."\n";
	$sum += ($i**$i);
}
print $sum."\n";