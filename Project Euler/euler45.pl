use warnings;
use strict;

my $running = 1;
my $num = 1;
my $increment = 2;
while ($running){
	if ($num > 40755){
		if (isHexagonal($num) && isPentagonal($num)){
			print $num."\n";
			exit;
		}
	}
	$num += $increment;
	$increment++;
}

sub isHexagonal{
	my $n = shift;
	return (((sqrt((8*$n) +1) +1) / 4) == int(((sqrt((8*$n) +1) +1) / 4)));
}
sub isPentagonal{
	my $n = shift;
	return (((sqrt((24*$n) +1) +1) / 6) == int(((sqrt((24*$n) +1) +1) / 6)));
}
