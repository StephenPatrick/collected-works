import sys
from primefac import gcd 
from math import sqrt
from time import clock
 
# def factorial_factors(n):


def factors(n):
        step = 2 if n%2 else 1
        return set(reduce(list.__add__, 
                    ([i, n//i] for i in range(1, int(sqrt(n))+1, step) if n % i == 0)))

def factorial(k):
    if k < 2: return 1
    return k * factorial(k-1)

def main(n):
	fac = factorial(n)
	divisors = factors(fac)
	total = 0
	for d in divisors:
		if gcd(d, fac/d) == 1:
			total += d**2
	print total

if __name__ == '__main__':
	s = clock()
	main(int(sys.argv[1]))
	print (clock() - s)