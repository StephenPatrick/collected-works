package permute

import (
  "sort"
  "strings"
  "bytes"
)

// Reverses a string
func Reverse(s string) string {
  runes := []rune(s)
  for i, j := 0, len(runes)-1; i < j; i, j = i+1, j-1 {
    runes[i], runes[j] = runes[j], runes[i]
  }
  return string(runes)
}

// Splits string into head and decreasing tail substrings
func splitHeadAndTail(str string) (string, string) {
  currentIndex := len(str) - 1
  currentChar := ""
  for currentIndex >= 0 {
    if str[currentIndex:currentIndex+1] < currentChar {
      currentIndex++
      break
    }
    currentChar = str[currentIndex : currentIndex+1]
    currentIndex--
  }

  if currentIndex < 0 {
    return "", str
  }
  return str[0:currentIndex], str[currentIndex:len(str)]
}

var buffer bytes.Buffer

// Sorts characters of a string
func sortString(w string) string {
    s := strings.Split(w, "")
    sort.Strings(s)
    return str_append(s...)
}

func str_append(strs ...string) string {
    buffer.Reset()
    for _, str := range strs {
        buffer.WriteString(str)
    }
    return buffer.String()
}

func byte_append(strs ...byte) string {
    buffer.Reset()
    for _, str := range strs {
        buffer.WriteByte(str)
    }
    return buffer.String()
}

// Get next lexicographic permutation in O(n) time where n is the length of the string
// http://wordaligned.org/articles/next-permutation
func NextPermutation(str string) string {
  if len(str) == 0 || len(str) == 1 {
    return str
  }
  head, tail := splitHeadAndTail(str)
  if head == "" {
    return str
  }
  headChars := []rune(head)
  tailChars := []rune(tail)
  currentIndex := len(tail) - 1
  for currentIndex >= 0 {
    if tail[currentIndex:currentIndex+1] > head[len(head)-1:len(head)] {
      temp := tailChars[currentIndex]
      tailChars[currentIndex] = headChars[len(head)-1]
      headChars[len(head)-1] = temp
      break
    }
    currentIndex--
  }
  return string(headChars) + Reverse(string(tailChars))
}

// Compute all lexicographic permutations of a string
func LexicographicPermutations(str string) []string {
  str = sortString(str)
  result := []string{str}
  last := str
  current := NextPermutation(str)
  for last != current {
    result = append(result, current)
    last = current
    current = NextPermutation(current)
  }
  return result
}

var memoize_cache = make(map[string]map[string]bool)

func RecursivePermutations(str string) map[string]bool {
    if (len(str) < 2) {
        m := map[string]bool{}
        m[str] = true
        return  m
    }
    str = sortString(str)
    if val, ok := memoize_cache[str]; ok {
        return val
    }
    partial := RecursivePermutations(str[:len(str)-1])
    result := map[string]bool{}
    for perm,_ := range partial {
        result[str_append(perm,string(str[len(str)-1]))] = true
        for i,_ := range perm {
            result[str_append(perm,string(str[len(str)-1]))] = true
            result[str_append(perm[:i],string(str[len(str)-1]),perm[i:])] = true    
        }
    }
    memoize_cache[str] = result
    return result
}