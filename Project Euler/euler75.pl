use warnings;
use strict;

# Need a faster algorithm
my @counts = (0) * ($ARGV[0]+1);
for (my $a = 1; $a < $ARGV[0]/2; $a++){
	for (my $b = $a; $b < $ARGV[0]/2; $b++){
		my $c = pythagoras($a,$b);
		if (($c == int($c)) && (($a+$b+$c) < $ARGV[0]+1)){
			$counts[$a+$b+$c]++;
		}
	}
}
my $total = 0;
for (my $i = 0; $i < scalar @counts; $i++){
	if (defined $counts[$i] && $counts[$i] == 1){
		$total += $counts[$i];
	}
}
print $total."\n";

sub pythagoras{
	my ($a, $b) = @_;
	return sqrt ($a**2 + $b**2);	
}