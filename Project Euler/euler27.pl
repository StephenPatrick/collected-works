use warnings;
use strict;

my @primes = Eratosthenes(1000000);
my $n = 0;
my $max = 0;
my $product = 0;
for (my $a = -999; $a < 1000; $a++){
	for (my $b = -997; $b < 1000; $b++){
		if ($primes[$b] == 1){
			$n = quad_primes($a,$b);
			if ($n > $max) { $max = $n; $product = $a * $b;}
		}
	}
} 
print $product;


sub quad_primes{
	my ($a, $b) = @_;
	my $return = 0;
	for (my $n = 0; $n < 1000; $n++){
		if (@primes[$n**2 + $a*$n + $b] == 0){
			$return = $n;
			last;
		}
	}
	if ($return == 0) {print "failure"; exit;}
	return $return;
}

sub Eratosthenes{
	my $n = shift;
	my @primes = (0,0);
	my @addon = (1) x ($n - 2);
	push @primes, @addon;
	for (my $i = 2; $i <= sqrt $n; $i++){
		if ($primes[$i] == 1){
			for (my $j = $i**2; $j <= $n; $j += $i){
				$primes[$j] = 0;
			}
		}
	}
	return @primes;
}