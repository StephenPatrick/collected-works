resolution_order = [[0,1,2],[0,2,1],[1,0,2],[1,2,0],[2,0,1],[2,1,0]]
math_order = []
for (a = 0; a < 4; a++) {
    for (b = 0; b < 4; b++) { 
        for (c = 0; c < 4; c++) {
            math_order.add([a,b,c])
        }
    }
}

div = {double x,double y->
    if (y == 0) return null;
    return (x/y).doubleValue()
}

math_func = [{x,y->x+y},{x,y->x-y},{x,y->x*y},div]
math_print = ["+","-","*","/"]

euler93 = {
    sets = [:]
    for (a = 1; a < 10; a++) {
        for (b = a+1; b < 10; b++) {
            for (c = b+1; c < 10; c++) {
                for (d = c+1; d < 10; d++) {
                    quit = false
                    results = [:]
                    base_elements = [a,b,c,d]
                    base_elements.eachPermutation {x_elements->
                        for (resolution in resolution_order) {
                            for (math in math_order) {
                                
                                int end = 0
                                quit = false
                                
                                List<Integer> elements = x_elements.clone()
                                for (resolve in resolution) {
                                    
                                    elements[resolve] = math_func[math[resolve]](elements[resolve],elements[resolve+1])
                                    elements[resolve+1] = elements[resolve]
                                    end = resolve
                                    if (elements[resolve] == null) {
                                        quit = true
                                        break
                                    }
                                }
                                if (quit) break
                                m = elements[end]
                                if (m == Math.round(m) && m > 0) {
                                    results[(int)Math.round(m)] = true
                                    if (base_elements == [3,5,7,8] && m < 65) {
                                        print x_elements[0]
                                        print math_print[math[0]]
                                        print x_elements[1]
                                        print math_print[math[1]]
                                        print x_elements[2]
                                        print math_print[math[2]]
                                        print x_elements[3]
                                        print " "
                                        print resolution
                                        print " "
                                        println elements[end]
                                    }
                                }
                            }
                        }
                    }
                    keys = results.keySet().toArray()
                    keys.sort()
                    sets[a.toString() + b + c + d] = keys 
                }
            }
        }
    }
    int longest = 0
    longest_i = "-1"
    //println sets
    sets.each{k,v ->
        len = 0
        // find how long we can go from 1 onward
        // The first element must be 1, and so forth
        for (x in v) {
            if (x != len + 1) break
            len++
        }
        if (len > longest) {

            longest = len
            longest_i = k
        }
    }
    println longest
    println longest_i
}

// This reports that 3578 outputs 65. 
// The second best answer it reports is what eulerproject accepts. 
// This could be due to floating point / rounding things? I'm not sure what the issue is.
// Worth looking into. Perhaps there is a requirement in the problem I'm not seeing.