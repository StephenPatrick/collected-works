#use warnings;
use strict;

my $value;
my $count;
my $max = 0;
my $maxindex;
my @list;
for (my $i = 1000; $i > 1; $i--){
	if ($max > $i){
		last;
	}
	@list = (0) * $i;
	$value = 1;
	$count = 0;
	while ($list[$value] == 0 && $value != 0){
		$list[$value] = $count;
		$value *= 10;
		$value %= $i;
		$count++;
	}
	if ($count - $list[$value] > $max){
		print $i."\n";
		$max = $count - $list[$value];
	}
}
print $max."\n";