package main

import "fmt"
import "math/big"
import "strings"
import "sort"
import "bytes"

// Sorts characters of a string
func sortString(w string) string {
	s := strings.Split(w, "")
	sort.Strings(s)
	return str_append(s...)
}

var buffer bytes.Buffer

func str_append(strs ...string) string {
	buffer.Reset()
	for _, str := range strs {
		buffer.WriteString(str)
	}
	return buffer.String()
}

func main() {

	gesamt := 0

	for ganzzahl := 1; ganzzahl < 10; ganzzahl++ {
		ganz := big.NewInt(int64(ganzzahl))
		hochzahl := int64(1)
		for true {
			// If it's faster, we could instead
			// repeatedly multiply ganz by itself.
			// (once each loop)
			val := big.NewInt(0)
			str := val.Exp(ganz, big.NewInt(hochzahl), nil).String()
			if int64(len(str)) < hochzahl {
				break
			}
			fmt.Println(str, ganzzahl, hochzahl)
			gesamt++
			hochzahl++
		}
	}

	fmt.Println(gesamt)
}
