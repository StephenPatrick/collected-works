from decimal import Decimal as dec
from math import floor

def f(x):
	return floor(dec(2)**(dec(30.403243784)*(dec(x)**dec(2)))) * 10**(-9)

def main():
	u = -1
	for i in xrange(20):
		print i
		print u
		u = f(u)

if __name__ == '__main__':
	main()