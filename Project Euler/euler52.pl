use warnings;
use strict;

my $i = 1;
my @counts = (0,0,0,0,0,0,0,0,0,0);
my @comp_counts = (0,0,0,0,0,0,0,0,0,0);
my $comp;
my $sstr;
my $testing;
my $running = 1;
while ($running){
	$testing = 1;
	if (length $i == length $i*6){
		while ($testing){
	
			for (my $j = 0; $j < length $i; $j++){
				$counts[(substr ($i, $j, 1))]++;
			}
			for (my $j = 2; $j < 6; $j++){
				$comp = $i * $j;
				my @comp_counts = (0,0,0,0,0,0,0,0,0,0);
				for (my $k = 0; $k < length $comp; $k++){
					$comp_counts[(substr ($comp, $k, 1))]++;
				}
				if (!(@counts ~~ @comp_counts)){
					$testing = 0;
					last;
				}
			}
			if ($testing == 1){
				print $i."\n";
				my $wait = <>;
				exit;
			}
		}
	}
	@counts = (0,0,0,0,0,0,0,0,0,0);
	$i++
}