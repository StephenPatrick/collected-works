use warnings;
use strict;
use Memoize;
use POSIX;
no warnings 'recursion';

memoize('divisors');
memoize('slow_totient');

my $x = farey_magnitude(12000);
# A little handwavey
# It'd be great to know if there was
# a more accurate filter which
# was just as simple
# The four here is {0/1, 1/1, 1/2, 1/3}
print floor($x - $x/2 - $x/3 - 4);



sub farey_magnitude{
    my $limit = shift;
    my $sum = 0;
    for (my $i = 1; $i <= $limit; $i++) {
        $sum += slow_totient($i)
    }
    return $sum;
}

sub slow_totient{
	my $base = shift;
	my @divisors = divisors($base,'divisors');
	#foreach (@divisors) {print $_." ";}
	#print "for $base.\n";
	if ($base < 2) {return 0;}
	if ($base == 2) {return 1;}
	my $increase = $base - 1;
	foreach (@divisors){
		$increase -= slow_totient($_);
	}
	return $increase;
}	

	
# Generate a divisor list and sum of all divisors for a
# given integer.
# Input: int, desired output
# Output: A) int sum of all divisors,
#	  B) int array of divisors,
#	  C) both A and B
sub divisors{
	my $base = shift;
	my $type = shift;
	my @divisors = ();
	my $divsum = 0;
	my $k = 0;
	
	# Add all factors under the square root of the base
	for (my $j=1;$j<sqrt $base + 1;$j++) {
		if ($base % $j == 0) {
			$divisors[$k] = $j;
			$divsum += $divisors[$k];
			$k++;
		}
	}
	# Determine all factors paired with those under the
	# square root.
	for (my $j=1;$j<(scalar @divisors);$j++) {
		# Exclude duplicate squares
		if (!($base / $divisors[$j] ~~ @divisors)) {
			$divisors[$k] = $base / $divisors[$j];
			$divsum += $divisors[$k];
			$k++;
		}
	}
	
	# Return desired output
	if ($type eq "sum"){
		return $divsum;
	}
	elsif ($type eq "both"){
		return (@divisors,$divsum);
	} 
	else {
		return @divisors;
	}
}