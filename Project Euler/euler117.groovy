tileCount234 = {squares->
    if (squares < 1) {
        return 0
    }
    long total = tileCount(squares,2)+tileCount(squares,3)+tileCount(squares,4)
    // What if we don't put a tile at the front
    return total + tileCount234(squares-1)
}.memoize()
tileCount = {squares,length->
    if (squares < length) {
        return 0
    }
    if (squares == length) {
        return 1
    }
    // What if we put a tile at the front
    return tileCount234(squares-length) + 1
}.memoize()