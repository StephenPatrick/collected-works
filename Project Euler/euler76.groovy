tileCountAllLengths = {squares, max ->
    println squares + " " + max
    if (squares < 0) {
        return 0
    } else if (squares == 0) {
        return 1
    }
    int total = 0
    for (int i = max; i > 0; i--) {
        total += tileCountAllLengths(squares-i,i)
    }
    return total
}.memoize()
tileCountAllLengths(100,99)