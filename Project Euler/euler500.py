from primefac import prime_generator as primes
import sys

def main(limit):

	fac_list = []

	p = primes()

	# Start with the first <limit> primes
	for i in xrange(limit):
		fac_list.append(next(p))

	counts = [1] * limit

	toAdd = 0
	toRemove = limit-1
	countToAdd = 2
	while toAdd < len(fac_list):
		if fac_list[toAdd] ** countToAdd < fac_list[toRemove]:
			counts[toAdd] += countToAdd
			countToAdd *= 2
			toRemove -= 1
			fac_list.pop()
		else:
			if countToAdd == 2:
				break
			countToAdd = 2
			toAdd += 1
	product = 1
	for i in xrange(len(fac_list)):
		product = (product * fac_list[i] ** counts[i]) % 500500507
	print product

if __name__ == '__main__':
	main(int(sys.argv[1]))