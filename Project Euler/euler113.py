import sys
from time import clock

def is_bouncy(n):
	s = str(n)
	prev = ord(s[0])
	increasing = False
	decreasing = False
	for c in s:
		if ord(c) > prev:
			increasing = True
		elif ord(c) < prev:
			decreasing = True 
		prev = ord(c)
	return increasing and decreasing
 
# Wow! We need to hit a googol, so this is never gonna happen
# We need a mathematic approach to removing large batches of numbers
# Using string composition would be a better idea than iterating
# through addition

def main(limit):
	not_bouncy = 0
	for i in xrange(1,limit):
		i += 1
		if not is_bouncy(i):
			not_bouncy += 1
	print not_bouncy

if __name__ == '__main__':
	main(int(sys.argv[1]))