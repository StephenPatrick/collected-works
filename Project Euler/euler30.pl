use warnings;
use strict;
use bignum;
use v5.10;

sub Fifth_Pow_Sum {
	my $n = shift;
	my $sum = 0;
	for (my $i = 0;$i < length substr ($n,0);$i++) {
 		my $x = substr ($n,$i,1);
 		$x = $x**5;
 		$sum += $x;
 		if ($sum > $n) {return 0}	 
	}
	return $sum;
}

my $sum = 0;
my $n = 0;
for (my $i=2;$i < 354294;$i++){
	$n = Fifth_Pow_Sum($i);
	say $n.", ".$i;
	if ($n == $i) {
		$sum += $i;
		say "WOOHOO";
	}
}
say "ANSWER: ".$sum;