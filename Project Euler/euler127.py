from time import clock
import sys
from primefac import distinct_primefactors, gcd, abc_hit
import csv
import profile

# file = open('e127.csv','wb')
# writer = csv.writer(file)
# writer.writerow(["a","b","c"])

# Consider iterating over prime factor sets instead?
# Right now we iterate over numbers, and then
# spend a lot of effor pulling prime factors out of those
# numbers.
# We could instead determine whether a set of prime factors
# exceeds our limit or not. 

def main(limit):
	start = clock()
	total = 0
	total_hits = 0
	for a in xrange(1,limit/2):
		print a
		for b in xrange(a+1,limit-a):
			if gcd(a,b) == 1:
				c = a + b
				if abc_hit(a*b*c,c,1):
					total_hits += 1
					total += c
	print total_hits
	print total
	print (clock() - start)

if __name__ == '__main__':
	main(int(sys.argv[1]))
	#main(int(sys.argv[1]))