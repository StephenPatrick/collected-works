import sys
from primefac import prime_generator as primes
from time import clock

def main(limit):

	solved = {}

	# Primes, and all values p^k
	# will always be 1
	p = primes()
	n = next(p)
	while n < limit:
		m = n
		while m < limit:
			solved[m] = 1
			m = m*n
		n = next(p)

	sum = 0
	for n in xrange(1,limit):
		if n in solved:
			sum += solved[n]
			continue
		for a in xrange(n-1,0,-1):
			if (a**2) % n == a:
				print str(n) + "," + str(a)
				sum += a
				break
	print sum

if __name__ == '__main__':
	s = clock()
	main(int(sys.argv[1]))
	print (clock() - s)