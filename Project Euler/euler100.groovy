// https://www.alpertron.com.ar/QUAD.HTM
euler100 = {->
    b = 15G
    c = 21G
    while (b + c < 1000000000000G) {
        b2 = 3*b + 2*c - 2
        c2 = 4*b + 3*c - 3
        b = b2
        c = c2
        println(b + " " + c)
    }
}
