package main

import "fmt"
import "math/big"
import "strconv"

func g_atoi(s string) int {
	i, _ := strconv.Atoi(s)
	return i
}

func main() {

	byte_ints := [...]byte{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'}
	processed := make(map[int64]bool)

	// Iterate until a number has 8 prime replacements
	for i := 1; i < 10000000; i += 2 {
		if processed[int64(i)] {
			continue
		}
		// Initialize a byte array for quick replacement.
		baseString := []byte(strconv.Itoa(i))
		// Could pre-process this.
		subsets := get_subsets(len(baseString))
		// Keep track of the longest series of primes so far.
		bestPrimeSet := make(map[int64]bool)
		// Iterate through every subset
		for _, set := range subsets {
			primeSet := make(map[int64]bool)
			for _, digit := range byte_ints {
				curString := []byte(strconv.Itoa(i))
				copy(curString, baseString)
				for _, elem := range set {
					curString[elem] = digit
				}
				if curString[0] == '0' {
					continue
				}
				intVal := int64(g_atoi(string(curString)))
				if big.NewInt(intVal).ProbablyPrime(2) {
					primeSet[intVal] = true
					// Desperately try to avoid duplicating work
					processed[intVal] = true
				}
			}
			// Update our best series
			if len(primeSet) > len(bestPrimeSet) {
				bestPrimeSet = primeSet
			}
		}
		if len(bestPrimeSet) > 5 {
			if len(bestPrimeSet) == 8 {
				break
			}
		}
	}
}

func subsets_rec(i int, offset int) [][]int {
	returnVal := [][]int{}
	offsetList := []int{offset}
	returnVal = append(returnVal, offsetList)
	// Base case
	if i == 1 {
		return returnVal
	}
	if i < 1 {
		return nil
	}
	laterVal := subsets_rec(i-1, offset+1)
	for j := 0; j < len(laterVal); j++ {
		newVal := append(laterVal[j], offset)
		returnVal = append(returnVal, newVal)
	}
	return append(returnVal, laterVal...)
}

func get_subsets(i int) [][]int {
	return subsets_rec(i, 0)
}
