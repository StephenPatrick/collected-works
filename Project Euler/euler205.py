from fractions import Fraction
from time import clock

# XdY, i.e. 2d6 would be dice_dist(2,6)
def dice_dist(x,y):
	if x == 0 or y == 0:
		return [1]
	dist = [0] * ((x*y)+1) # x*y is the maximum result
	for res in xrange(1,y+1):
		dist[res] = Fraction(1,y)

	for die in xrange(x-1):
		newDist = [0] * ((x*y)+1)
		for res in xrange(1,((x-1)*y)+1):
			if dist[res] != 0:
				for add in xrange(1,y+1):
					newDist[res+add] += dist[res] * Fraction(1,y)

		dist = newDist
	return dist

# Generate a dice distribution but turn all values into integers
# and package them with their common denominator.
def dice_dist_noFrac(x,y):
	denom = y**x
	dist = dice_dist(x,y)
	for res in xrange(len(dist)):
		dist[res] *= denom
		dist[res] = dist[res].numerator
	return [dist, denom] 

# Returns a distribution that expresses how many instances
# are greater than a given value from an existing distribution
def greater_distribution(dist, denom):
	for x in xrange(len(dist)):
		denom -= dist[x]
		dist[x] = denom
	return dist

def main():
	s = clock()
	(peter, p_denom) = dice_dist_noFrac(9,4)
	peter = greater_distribution(peter, p_denom)
	(colin, c_denom) = dice_dist_noFrac(6,6)

	new_denom = c_denom * p_denom
	new_numer = 0
	for i in xrange(len(peter)):
		new_numer += colin[i] * peter[i]
	print new_numer / float(new_denom)
	print (clock() - s)
	
if __name__ == '__main__':
	main()