use warnings;
use strict;

my @divisor_pairs;
my $string;
my $sum = 0;
for (my $i = 1000; $i < 10000; $i++){
	if ($i =~ /^(?!.*([1-9]).*\1)[1-9]{4}$/){
	 @divisor_pairs = divisor_pairs($i);
	 for (my $j = 0; $j < scalar @divisor_pairs; $j+=2){
		  $string = $i.$divisor_pairs[$j].$divisor_pairs[$j+1];
		  if ($string =~ /^(?!.*([1-9]).*\1)[1-9]{9}$/){
			   $sum += $i;
			   last;
		  }
	 }
	}
}
print $sum."\n";

sub divisor_pairs{
 my $base = shift;
 my @divisor_pairs = ();
 my $k = 0;
 
 # Add all factors under the square root of the base
 for (my $j=1;$j<sqrt $base + 1;$j++) {
 	 if ($base % $j == 0) {
 	 	 $divisor_pairs[$k] = $j;
 	 	 $divisor_pairs[$k+1] = $base / $j;
 	 	 $k += 2;
 	 }
 }

 return @divisor_pairs;
}
