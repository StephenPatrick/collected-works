valToChar = [1:"I",5:"V",10:"X",50:"L",100:"C",500:"D",1000:"M"]

slice = {s,start,end=-1->
    if (start >= s.length()) {
        return "";
    }
    return s[start..end]
}


// Yes this can be done simpler
// This is more fun
// and more useful
readRoman = {s->
    sum = 0
    // Remove all 1000s
    while (s.length() > 0 && s[0] == valToChar[1000]) {
        s = slice(s,1)
        sum += 1000
    }

    // Partial 1000s
    thousand = s.indexOf(valToChar[1000])
    if (s.length() > 0 && thousand != -1) {
        if (s[0] == valToChar[500]) {
            sum += 500
        } else {
            sum += (1000 - (100*thousand))
        }
        s = slice(s,thousand+1)
    }
    
    // Further 500s
    five_hundred = s.indexOf(valToChar[500])
    if (s.length() > 0 && five_hundred != -1) {
        s = slice(s,five_hundred+1)
        sum += 500 - (five_hundred*100)    
    }

    // Remove all 100s
    while (s.length() > 0 && s[0] == valToChar[100]) {
        s = slice(s,1)
        sum += 100
    }
    
    // Partial 100s
    hundred = s.indexOf(valToChar[100])
    if (s.length() > 0 && hundred != -1) {
        if (s[0] == valToChar[50]){
            sum += 50
        } else {
            sum += 100 - (10*hundred)
        }
        s = slice(s,hundred+1)
    }
    
    // Further 50s
    fifty = s.indexOf(valToChar[50])
    if (s.length() > 0 && fifty != -1) {
        s = slice(s,fifty+1)
        sum += 50 - (fifty*10)    
    }
    
    // Remove all 10s
    while (s.length() > 0 && s[0] == valToChar[10]) {
        s = slice(s,1)
        sum += 10
    }

    // Partial 10s
    ten = s.indexOf(valToChar[10])
    if (s.length() > 0 && ten != -1) {
        if (s[0] == valToChar[5]) {
            sum += 5
        } else {
            sum += 10 - ten
        }
        s = slice(s,ten+1)
    }
    
    // Further 5s
    five = s.indexOf(valToChar[5])
    if (s.length() > 0 && five != -1) {
        s = slice(s,five+1)
        sum += 5 - five
    }
    
    // Remove all trailing 1s
    while (s.length() > 0 && s[0] == valToChar[1]) {
        s = slice(s,1)
        sum += 1
    }

    return sum
}

writeRoman = {n->
    out = ""
    while (n > 999) {
        out += valToChar[1000]
        n -= 1000
    }
    if (n > 899) {
        out += valToChar[100]
        out += valToChar[1000]
        n -= 900
    } else if (n > 499) {
        out += valToChar[500]
        n -= 500
    } else if (n > 399) {
        out += valToChar[100]
        out += valToChar[500]
        n -= 400
    }
    while (n > 99) {
        out += valToChar[100]
        n -= 100
    }
    
    if (n > 89) {
        out += valToChar[10]
        out += valToChar[100]
        n -= 90
    } else if (n > 49) {
        out += valToChar[50]
        n -= 50
    } else if (n > 39) {
        out += valToChar[10]
        out += valToChar[50]
        n -= 40
    }
    while (n > 9) {
        out += valToChar[10]
        n -= 10
    }
    
    if (n > 8) {
        out += valToChar[1]
        out += valToChar[10]
        n -= 9
    } else if (n > 4) {
        out += valToChar[5]
        n -= 5
    } else if (n == 4) {
        out += valToChar[1]
        out += valToChar[5]
        n -= 4
    }
    while (n > 0) {
        out += valToChar[1]
        n -= 1
    }
    return out
}

euler89 = {->
    file = new File("euler89-input.txt")
    int old_char_count = 0
    int new_char_count = 0
    file.eachLine { line ->
        println line
        x = readRoman(line)
        y = writeRoman(x)
        old_char_count += line.length()
        new_char_count += y.length()
    }
    println old_char_count - new_char_count
}