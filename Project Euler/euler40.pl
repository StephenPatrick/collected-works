use warnings;
use strict;

my $string = "";
my $num = 1;
while (length $string < 1000000){
	$string.= $num;
	$num++;
}
my $out = substr $string, 0, 1;
$out *= substr $string, 9, 1;
$out *= substr $string, 99, 1;
$out *= substr $string, 999, 1;
$out *= substr $string, 9999, 1;
$out *= substr $string, 99999, 1;
$out *= substr $string, 999999, 1;

print $out;