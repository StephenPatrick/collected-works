package main

import "fmt"
import "math/big"

func main() {

	//                 6789 will be the last unique input
	for i := 1234; i < 6790; i++ {
		perms := []int{}
		permute_int(i, func(p []int) {
			perms = append(perms, digit_slice_to_int(p))
		})
		//fmt.Println(perms);
		for j := 0; j < len(perms); j++ {
			for k := 0; k < len(perms); k++ {
				if j == k || perms[j] == perms[k] {
					continue
				}
				for h := 0; h < len(perms); h++ {
					if k == h || j == h || perms[j] == perms[h] || perms[h] == perms[k] {
						continue
					}
					if perms[j]-perms[k] == perms[k]-perms[h] {
						if big.NewInt(int64(perms[k])).ProbablyPrime(2) {
							if big.NewInt(int64(perms[j])).ProbablyPrime(2) {
								if big.NewInt(int64(perms[h])).ProbablyPrime(2) {
									fmt.Println(perms[j], perms[k], perms[h])
								}
							}
						}
					}
				}
			}
		}
	}
}

func permute_int(i int, emit func([]int)) {
	slice := []int{}
	for i > 0 {
		digit := i % 10
		i = i / 10
		slice = append(slice, digit)
	}
	permute(slice, emit)
}

func digit_slice_to_int(digits []int) int {
	sum := 0
	mult := 1
	for i := len(digits) - 1; i >= 0; i-- {
		sum += digits[i] * mult
		mult *= 10
	}
	return sum
}

// NOT MINE
// permute function.  takes a set to permute and a function
// to call for each generated permutation.
func permute(s []int, emit func([]int)) {
	if len(s) == 0 {
		emit(s)
		return
	}
	// Steinhaus, implemented with a recursive closure.
	// arg is number of positions left to permute.
	// pass in len(s) to start generation.
	// on each call, weave element at pp through the elements 0..np-2,
	// then restore array to the way it was.
	var rc func(int)
	rc = func(np int) {
		if np == 1 {
			emit(s)
			return
		}
		np1 := np - 1
		pp := len(s) - np1
		// weave
		rc(np1)
		for i := pp; i > 0; i-- {
			s[i], s[i-1] = s[i-1], s[i]
			rc(np1)
		}
		// restore
		w := s[0]
		copy(s, s[1:pp+1])
		s[pp] = w
	}
	rc(len(s))
}
