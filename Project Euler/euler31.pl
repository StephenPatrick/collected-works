use warnings;
use strict;
use Memoize;

my @coins = (1,2,5,10,20,50,100,200);
memoize('coin_sum');
print coin_sum(200,201)."\n";


sub coin_sum{
	my ($n, $last) = @_;
	my $sum = 0;
	if ($n < 0) {return 0;}
	elsif ($n == 0) {return 1;}
	else {
		for (my $i = 7; $i >= 0; $i--){
			if ($last >= $coins[$i]){
				$sum += coin_sum($n-$coins[$i],$coins[$i]);
			}
		}
		return $sum;
	}
}