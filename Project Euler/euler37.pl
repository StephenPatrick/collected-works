use warnings;
use strict;

my @bool_primes = Eratosthenes(1000000);
my @primes = ();
for (my $i = 0; $i < scalar @bool_primes; $i++){
	if ($bool_primes[$i] == 1){
		push @primes, $i;
	}
}
my $sum = 0;
my $prime;
my $iterating;
for (my $i = 4; $i < scalar @primes; $i++){
	$prime = $primes[$i];
	$iterating = 1;
	while ($iterating == 1){
		$prime = substr $prime, 1;
		if ($bool_primes[$prime] != 1){
			$iterating = -1;
			last;
		}
		if (length $prime == 1) {
			$iterating = 0;
			last;
		}
	}
	if ($iterating == -1){
		next;
	}
	$prime = $primes[$i];
	$iterating = 1;
	while ($iterating == 1){
		$prime = substr $prime, 0, -1;
		if ($bool_primes[$prime] != 1){
			$iterating = -1;
			last;
		}
		if (length $prime == 1) {
			$iterating = 0;
			last;
		}
	}
	if ($iterating == -1){
		next;
	}
	$sum += $primes[$i];
	print $primes[$i]."\n";
}
print $sum."\n";

sub Eratosthenes{
	my $n = shift;
	my @primes = (0,0);
	my @addon = (1) x ($n - 2);
	push @primes, @addon;
	for (my $i = 2; $i <= sqrt $n; $i++){
		print $i."\n";
		if ($primes[$i] == 1){
			for (my $j = $i**2; $j <= $n; $j += $i){
				$primes[$j] = 0;
			}
		}
	}
	return @primes;
}